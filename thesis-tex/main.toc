\contentsline {part}{I\hspace {1em}Theoretical beauty}{4}{part.1}%
\contentsline {part}{II\hspace {1em}Experimental beauty: The \mbox {LHCb}\xspace detector}{5}{part.2}%
\contentsline {part}{III\hspace {1em}Analysis beauty: The \mbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}^0}}\xspace \!\ensuremath {\rightarrow }\xspace {\ensuremath {\ensuremath {\mu }\xspace ^+}}\xspace {\ensuremath {\ensuremath {\mu }\xspace ^-}}\xspace }}\xspace analysis}{6}{part.3}%
\contentsline {chapter}{\numberline {1}Analysis overview}{7}{chapter.1}%
\contentsline {chapter}{\numberline {2}Selection and efficiencies}{8}{chapter.2}%
\contentsline {chapter}{\numberline {3}BDT calibration}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}\mbox {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace \!\ensuremath {\rightarrow }\xspace \ensuremath {h}\xspace ^+ \ensuremath {h}\xspace '^-}}\xspace calibration}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Relative fitted yields}{10}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Relative trigger efficiencies}{11}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Relative PID efficiencies}{13}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Final results}{14}{subsection.3.1.4}%
\contentsline {section}{\numberline {3.2}\mbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}^0}}\xspace \!\ensuremath {\rightarrow }\xspace {\ensuremath {\ensuremath {\mu }\xspace ^+}}\xspace {\ensuremath {\ensuremath {\mu }\xspace ^-}}\xspace }}\xspace calibration}{14}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}MC-data weights}{15}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Occupancy correction}{16}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Final results}{20}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Cross-check}{20}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Corrections to the relative yields per BDT bin}{21}{section.3.3}%
\contentsline {chapter}{\numberline {4}Normalisation}{25}{chapter.4}%
\contentsline {chapter}{\numberline {5}Branching fraction measurement}{26}{chapter.5}%
\contentsline {chapter}{References}{26}{chapter.5}%
