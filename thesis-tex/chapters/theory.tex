\clearpage 
\newpage
\chapter{Theoretical framework}
\label{ch:theory} 
Decays of \bquark-mesons into two leptons, such as \Bmm decays, are interesting laboratories in the search for New Physics scenarios. In the Standard Model, these decays are predicted to be very rare processes mediated by the weak interaction, presented in Sec.~\ref{sec:weak-interaction}. They are flavour-changing neutral current processes that can only take place through so-called quantum loop transitions, involving corresponding loop and weak interaction suppression factors. They are also affected by the so-called GIM mechanism and helicity suppression, further decreasing their decay rates.

Another quality that makes the \Bmm processes of high interest is the presence of only leptons in the final state, while the hadronic interaction is restricted to the initial \bquark-hadron. This factorization results in a clean theoretical computation of their branching fraction, which is performed using an effective field approach described in Sec.~\ref{sec:effective-hamiltonian}. Following the general expression obtained from the effective field approach, the Standard Model prediction is calculated to a high precision level. Consequently, any deviations from the measured branching fraction relative to the Standard Model value will serve as hints of physics beyond the Standard Model. In order to constrain New Physics models, the \Bmm branching fraction values are generally combined with other complementary measurements showing deviations from the Standard Model, such as the rare decay measurement involving $\B\to K^{(*)}l^+l^-$ decays~\cite{LHCb:2021trn}.

Photon emission is inevitable in \Bmm decays. This photon radiation must be taken into account when calculating their decay branching fractions as well as in their experimental measurement. The two types of photon emission in the \Bmm processes are presented in Sec.~\ref{sec:radiative-corrections}. Moreover, the effect of neutral \B meson mixing is also considered in Sec.~\ref{sec:theory-vs-experiment}, where the difference between the theoretical and experimental approaches is discussed and their relation presented. 

\section{Standard Model: Weak interaction}
\label{sec:weak-interaction}
The weak interaction is mediated by the \W and \Z bosons, which only couple to left-handed fermions and right-handed antifermions, resulting in the breaking of parity symmetry\cite{Salam:1968rm,GLASHOW1961579,Weinberg:1967tq}. Moreover, processes mediated by the \Wpm bosons cause the quarks to change their flavour to another type. This process is described in the interaction basis of the quarks with the charged current lagrangian term
\begin{equation}
 \mathcal{L}_{CC} = -\frac{g}{\sqrt{2}}\overline{u_{iL}^{I}}\gamma_{\mu}W^{-\mu}d_{iL}^{I} - \frac{g}{\sqrt{2}}\overline{d_{iL}^{I}}\gamma_{\mu}W^{+\mu}u_{iL}^{I} 
 \label{eq:lagrangian-cc-no-mass}
\end{equation}
\noindent where $g$ is the coupling constant of weak interaction and $u_{iL}^{I}$ ($d_{iL}^{I}$) stands for the Dirac spinors of the left-handed up-type (down-type) quarks in the weak interaction basis, denoted by the superscript $I$. The subscript $i$ indicates the three generations. The presence of two terms in Eq.~\ref{eq:lagrangian-cc-no-mass} follows from the existence of two different charged currents mediated by oppositely charged \W bosons: the \Wp and \Wm bosons. 

The propagation states of the quarks are different from the interaction basis used in Eq.~\ref{eq:lagrangian-cc-no-mass}. Quark states acquire mass from their interaction with the Higgs field, described by the Yukawa coupling~\cite{Yukawa:1935xg}. From this coupling, a relation between the mass eigenstates, denoted as $d_{i}$ and $u_{i}$, and the interaction eigenstates, $d^{I}_{j}$ and $u^{I}_{j}$, is determined. By convention, the interaction and mass eigenstates are equal for the up-type quarks and the change of basis is absorbed by the down-type quarks as~\cite{Kobayashi:1973fv}
\begin{equation}
 \begin{array}{rcl}
 u_{i}^{I}&=&u_{j} \\
 d_{i}^{I}&=&V_{CKM,ij}d_{j} \\ %(V_L^uV_L^{d\dagger})
\end{array}
\label{eq:relation-mass}
\end{equation}
\noindent where $V_{CKM,ij}$ are the components of the Cabibbo-Kobayashi-Maskawa (CKM) matrix~\cite{Kobayashi:1973fv} implementing flavour changing among different quark types in the charged current. To explicitly show the quark mixing relations, Eq.~\ref{eq:relation-mass} can be written in matrix form as
\begin{equation}
 \begin{pmatrix}
 d^{I} \\
 s^{I} \\
 b^{I}
 \end{pmatrix}
 = 
 \begin{pmatrix}
 V_{ud} & V_{us} & V_{ub} \\ 
 V_{cd} & V_{cs} & V_{cb} \\ 
 V_{td} & V_{ts} & V_{tb}
 \end{pmatrix}
 \begin{pmatrix}
 d \\ 
 s \\ 
 b
 \end{pmatrix}
 \label{eq:ckm-relation}
\end{equation}
\indent The magnitudes of the CKM matrix elements are determined from the combination of several experimental measurements resulting in~\cite{PDG}
\begin{equation}
 \begin{pmatrix}
 |V_{ud}| & |V_{us}| & |V_{ub}| \\ 
 |V_{cd}| & |V_{cs}| & |V_{cb}| \\ 
 |V_{td}| & |V_{ts}| & |V_{tb}|
 \end{pmatrix}
 =
 \begin{pmatrix}
 0.97401 \pm 0.00011& 0.22650 \pm 0.00048 & 0.00361^{+0.00011}_{-0.00009} \\ 
 0.22636 \pm 0.00048 & 0.97320 \pm 0.00011 & 0.04053^{+0.00083}_{-0.00061} \\ 
 0.00854^{+0.00023}_{-0.00016} & 0.03978^{+0.00082}_{-0.00060} & 0.999172^{+0.000024}_{-0.000035}
 \end{pmatrix}
 \label{eq:ckm-sizes}
\end{equation}
\indent Since the quark mass eigenstates are the propagation states as observed in nature, the lagrangian from Eq.~\ref{eq:lagrangian-cc-no-mass} can be expressed in the mass basis as 
\begin{equation}
 \mathcal{L}_{CC} = -\frac{g}{\sqrt{2}}\overline{u_{iL}}V_{CKM,ij}\gamma_{\mu}W^{-\mu}d_{jL} - \frac{g}{\sqrt{2}}\overline{d_{iL}}\gamma_{\mu}V_{CKM,ij}^*W^{+\mu}u_{jL} 
 \label{eq:lagrangian-cc-mass}
\end{equation}
\noindent such that the lagrangian is expressed in the same basis as the Yukawa coupling, giving rise to the quark masses. By doing that, the quark mixing is explicitly included in the charged current Lagrangian. 

\subsection{Suppression of the \texorpdfstring{\Bmm}{BtoMuMu} decays in the Standard Model}
The \Bmm process requires the change of flavour between two down-type quarks, the $d(s)$ and the $b$ quarks. Such transitions are known as \textit{flavour changing neutral currents (FCNC)} because the charge of the two quarks is the same. These transitions are not allowed at tree level in the Standard Model since neutral currents only occur at tree level by the exchange of a \Z boson between two same-type quarks. Instead, FCNC processes are only allowed in the Standard Model through loop transitions involving multiple charge currents; hence, they are highly suppressed.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=\textwidth]{images/Theory/feynman_diagrams}
 \caption{Standard Model Feynman diagrams of the lowest order contributions to \Bmm decays: the penguin diagram (left) and the box diagram (right). A second box diagram contributes to the process obtained by replacing the \W lines with \uquark, \cquark, \tquark lines and vice versa.}
 \label{fig:feynman-bmm}
\end{figure}

The two main transitions contributing to the FCNC \Bmm decays are displayed in the Feynman diagrams from Fig.~\ref{fig:feynman-bmm}. As with the rest of FCNC transitions, they are suppressed by the loop suppression factor $(g/4\pi)^2\approx 10^{-3}$ arising from the two additional vertices needed in the process compared to tree-level transitions. At each vertex involving quark mixing, the corresponding CKM matrix element is added to the amplitude, further suppressing the \Bmm processes, as can be seen from their magnitudes in Eq.~\ref{eq:ckm-sizes}. Moreover, virtual contributions from all up-type quarks in the loop contribute to the process such that the decay amplitude includes three terms with relative magnitudes according to: 
\begin{equation}
 \mathcal{A}(\Bmm) \propto V_{uq} V_{ub}^{*}\left (\frac{1}{q^2-m_u^2}\right ) + V_{cq} V_{cb}^{*} \left (\frac{1}{q^2-m_c^2}\right ) + V_{tq} V_{tb}^{*} \left (\frac{1}{q^2-m_t^2}\right )
\end{equation}
\indent Due to the CKM unitarity condition, giving $V_{uq} V_{ub}^{*} + V_{cq} V_{cb}^{*} + V_{tq} V_{tb}^{*} = 0$, the \Bmm processes would be completely forbidden if the masses of the quarks would be the same. Fortunately, a complete cancellation is avoided thanks to the different masses of the quarks, a procedure known as the \textit{GIM mechanism}~\cite{Glashow:1970gm}.

Angular momentum conservation adds an additional source of suppression known as \textit{helicity suppression}. The total angular momentum of the \Bds mesons is 0 since they are spin-0 particles with no angular momentum in the ground state. Due to angular momentum conservation, the total angular momentum of the final state must also be zero. This condition requires the final-state muons to have opposite spins and, hence, the same helicity in the centre of mass frame, as illustrated in Fig.~\ref{fig:helicity-suppression}. However, weak interaction only couples with left-handed (right-handed) particles (antiparticles), resulting in the muons from the decay having opposite chirality. Since chirality and helicity are identical in the ultra-relativistic limit, \Bmm decays would be forbidden if muons were massless particles. Although intimately related, chirality and helicity are not identical for massive particles, lifting the cancellation of the \Bmm process but adding a helicity suppression factor in the amplitude of the process of $(m_{\mu}/M_{\Bds})^2 \approx 4 \times 10^{-4}$.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.8\textwidth]{images/Theory/helicity_suppression}
 \caption{Illustration of the \Bmm decay in the centre-of-mass frame resulting from the angular momentum conservation. The direction of the momenta of the muons is shown in black and their spin in grey. The helicity of the particles depending on the spin and the momentum are illustrated in the box.}
 \label{fig:helicity-suppression}
\end{figure}

Due to the loop suppression factors, the multiple weak interactions, the GIM mechanism and the helicity suppression, only 3 out of every billion \Bs mesons are expected to decay to two muons in the Standard Model~\cite{Beneke:2019slt}. The number of \Bd decaying to two muons is expected to be around 1 out of 10 billion \Bd mesons~\cite{Beneke:2019slt}.

\section{Effective hamiltonian for weak interactions}
\label{sec:effective-hamiltonian}
The energy transfer in \B meson decays is at the order of the \bquark-quark mass, much smaller than the masses of the \W and \Z bosons mediating the decay. As a consequence, the effect of the propagators is negligible in the process such that they are "integrated out" of the theory and replaced by a four-point interaction containing all the perturbative information of the process. In this {effective field} description~\cite{Cohen:2019wxr}, the weak mediators are then no longer degrees of freedom of the theory and the contributions to the \Bmm processes are reduced to a four-point interaction.

Within the effective field description, the Hamiltonian describing the particle processes can be separated into two parts: the perturbative effects independent of the initial and final state particles are encoded in the Wilson coefficients $\mathcal{C}_i$, while the long-distance part is parametrized with the Wilson operators, $\mathcal{O}_i$. In the case of the \Bmm decay, the effective field hamiltonian~\cite{Buchalla:1995vs} is given by 
\begin{equation}
 \mathcal{H}_{eff} = - \frac{G_F\alpha}{\sqrt{2\pi}} \left (V_{tq}^*V_{tb} \sum_{i} \left ( \mathcal{C}^{(\prime)}_{i}\mathcal{O}^{(\prime)}_{i} \right ) \right )
 \label{eq:effective-hamiltonian}
\end{equation}
\noindent where $G_F$ is the Fermi coupling constant, $\alpha$ is the electromagnetic coupling constant and $V_{tq}^*$ and $V_{tb}$ the dominating CKM matrix elements present in the \Bmm process. Using the effective hamiltonian, the amplitude for the \Bmm decays is obtained as 
\begin{equation}
 \mathcal{M}(\Bmm) = \braket{\mup\mun|\mathcal{H}_{eff}|\Bds} = - \frac{G_F\alpha}{\sqrt{2}\pi} V_{tq}^*V_{tb} \sum_{i} \mathcal{C}_{i}^{(\prime)}\braket{\mup\mun|\mathcal{O}_{i}^{(\prime)}|\Bds} 
 \label{eq:matrix-element-general}
\end{equation}
\indent The hadronic and leptonic parts of the \Bmm decay are well separated and restricted to the initial and final state, respectively. Since there is no direct interaction between quarks and leptons, the decay amplitude is factorized into the leptonic and hadronic parts
\begin{equation}
 \braket{\mup\mun|\mathcal{O}_{i}^{(\prime)}|\Bds} = \braket{\mup\mun|\mathcal{O}_{i, ll}^{(\prime)}|0} \braket{0|\mathcal{O}_{i,qq}^{(\prime)}|\Bds}
\end{equation}
\noindent where $\mathcal{O}_{i, ll}^{(\prime)}$ and $\mathcal{O}_{i,qq}^{(\prime)}$ are the leptonic and hadronic components of the four-point operator. This factorization restricts the non-perturbative QCD effects to the initial state, allowing for a clean theoretical computation of the \Bmm branching fraction.

The specific form of the operators results from the combination of all the possible leptonic and hadronic currents defined by the theory~\cite{Buchalla:1995vs}. Due to the pseudoscalar nature of the \Bds meson, the only operators contributing to the \Bmm decay are~\cite{Buras:2013uqa,Knegjens}:
\begin{align}
 &\mathcal{O}_{10} = (\quarkbar\gamma_{\mu}P_L\bquark)(\overline{l}\gamma^{\mu}\gamma_5 l) &&\mathcal{O}_{10}^\prime = (\quarkbar\gamma_{\mu}P_R\bquark)(\overline{l}\gamma^{\mu}\gamma_5 l) \nonumber \\
 & \mathcal{O}_{S} = m_{\bquark}(\quarkbar P_R\bquark)(\overline{l} l) &&\mathcal{O}_{S}^\prime = m_{\bquark}(\quarkbar P_L\bquark)(\overline{l} l) \label{eq:operators-eft} \\
 &\mathcal{O}_{P} = m_{\bquark}(\quarkbar P_R\bquark)(\overline{l} \gamma_5 l) &&\mathcal{O}_{P}^\prime= m_{\bquark}(\quarkbar P_L\bquark)(\overline{l} \gamma_5 l) \nonumber
\end{align}
\noindent where $\quark = \squark, \dquark$ and $P_{L}=\frac{1}{2}(1-\gamma_5)$, $P_{R}=\frac{1}{2}(1+\gamma_5)$ are the chirality projectors such that chirality is flipped in the primed operators. These operators represent the Lorentz structure of the mediators contributing to the decay process that was integrated out. The $\mathcal{O}_{10}$ is related to the axial vector lepton currents, such as the electroweak penguin shown in Fig.~\ref{fig:feynman-bmm}. The operators $\mathcal{O}_{S,P}$ are (pseudo)-scalar lepton currents as penguin or box diagrams mediated by (pseudo) scalar particles such as the Higgs or possible contributions from hypothesized new particles.


\subsection{Hadronic matrix elements evaluation}
The hadronic part of the matrix element related to the four-point operator $\mathcal{O}_{10}$ is split into two parts:
\begin{equation}
 \braket{0|\mathcal{O}^{(\prime)}_{10, qq}|\Bds} = \frac{1}{2} \left [ \braket{0|\quarkbar\gamma^\mu \bquark|\Bds} \mp \braket{0|\quarkbar\gamma^\mu \gamma_5 \bquark|\Bds} \right ]
 \label{eq:o-10-qq-splitting}
\end{equation}
\noindent where the factor of 1/2 and the sign in the second term is given by the chiral projector, such that it is plus for $\mathcal{O}^{\prime}_{10, qq}$ and minus for $\mathcal{O}_{10, qq}$. Because parity is conserved in the strong interactions, the hadronic matrix elements should result in pseudoscalar or axial vector currents due to the pseudoscalar nature of the \Bds meson. Hence, the first term in Eq.~\ref{eq:o-10-qq-splitting} vanishes as it is a vector current. The remaining term must be proportional to the \Bds four-momentum since it is the only variable available describing the \Bds meson. Non-perturbative effects are included in the decay constant $f_{\Bds}$ such that the hadronic axial current is given by~\cite{Knegjens}
\begin{equation}
 \braket{0|\mathcal{O}^{(\prime)}_{10, qq}|\Bds} = \mp \frac{1}{2} \braket{0|\quarkbar\gamma^\mu \gamma_5 \bquark|\Bds} = \pm if_{\Bds}p^{\mu}
 \label{eq:hadronic-o-10}
\end{equation}

The scalar and pseudoscalar hadronic matrix elements are also split into two parts:
\begin{equation}
 \braket{0|\mathcal{O}^{(\prime)}_{S, P, qq}|\Bds} = \frac{m_\bquark}{2} \left [ \braket{0|\quarkbar\bquark|\Bds} \pm \braket{0|\quarkbar \gamma_5 \bquark|\Bds} \right ]
 \label{eq:s-p-hadronic-parts}
\end{equation}
\noindent where the $m_b$ factor comes from the definition of the scalar and pseudoscalar operators (see Eq.~\ref{eq:operators-eft}), and the factor of 1/2 and the sign in the second term is given by the chiral projector, such that it is plus for $\mathcal{O}_{S,P, qq}$ and minus for $\mathcal{O}^{\prime}_{S,P, qq}$. The first term in Eq.~\ref{eq:s-p-hadronic-parts} vanishes due to the pseudoscalar nature of the \Bds meson. To obtain the second term, Eq.~\ref{eq:hadronic-o-10} is contracted with the four-momentum of the \Bds meson, given by $p_{\mu}=p_{\quarkbar, \mu}+p_{\bquark, \mu}$, and using the relation $p^{\mu}p_\mu = M_{\Bds}$:
\begin{gather}
 \braket{0|\quarkbar p_\mu \gamma^\mu \gamma_5 \bquark|\Bds} = if_{\Bds}M_{\Bds}^2 \nonumber \\ 
 \braket{0|\quarkbar p_{\quarkbar,\mu} \gamma^\mu \gamma_5 \bquark|\Bds} - \braket{0|\quarkbar \gamma_5 p_{\bquark,\mu} \gamma^\mu \bquark|\Bds} = if_{\Bds}M_{\Bds}^2 \nonumber \\ 
 -(m_{\quarkbar}+m_{\bquark})\braket{0|\quarkbar \gamma_5 \bquark|\Bds} = if_{\Bds}M_{\Bds} \nonumber \\
 \braket{0|\quarkbar \gamma_5 \bquark|\Bds} = -if_{\Bds}\frac{M_{\Bds}^2}{m_{\quarkbar}+m_{\bquark}}
 \label{eq:hadronic-o-S}
\end{gather}
\noindent where the the Dirac equations for particles, $(p_{\mu}\gamma^\mu - m) \psi = 0$, and antiparticles, $(p_{\mu}\gamma^\mu + m) \overline{\psi} = 0$, have been used. Notice the minus sign included in the second line of the equation, resulting from the anticommutation properties of the $\gamma$ matrices. Combining Eq.~\ref{eq:s-p-hadronic-parts} and Eq.~\ref{eq:hadronic-o-S}, the hadronic scalar and pseudoscalar matrix element is given by
\begin{equation}
 \braket{0|\mathcal{O}^{(\prime)}_{S, P, qq}|\Bds} = \mp if_{\Bds}\frac{M_{\Bds}^2}{2}\frac{m_\bquark}{m_{\quarkbar}+m_{\bquark}}
\end{equation}

\subsection{Leptonic matrix elements evaluation}
The leptonic matrix element for the scalar and pseudoscalar contributions is obtained for the individual helicities using spinor contraction, resulting in~\cite{Knegjens}:
\begin{gather}
 \braket{\mup\mun| \mathcal{O}_S^{(\prime)}|0} = \braket{\mup\mun|\overline{l}l|0} = - M_\Bds\sqrt{1-4\frac{m_\mu^2}{M_\Bds^2}} \nonumber \\
 \braket{\mup\mun| \mathcal{O}_P^{(\prime)}|0} = \braket{\mup\mun|\overline{l}\gamma_5l|0} = \eta_\lambda M_\Bds
 \label{eq:scalar-pseudoscalar-leptonic}
\end{gather}
\noindent where $\eta_\lambda$ indicates the helicity of the two muon spinors, such that it is $\eta_\lambda =+1$ for right-handed and $\eta_\lambda =-1$ for left-handed spinors.

The leptonic matrix element for $\mathcal{O}^{(\prime)}_{10}$ is contracted with the hadronic part given in Eq.~\ref{eq:hadronic-o-10}. Due to four-momentum conservation $p^{\mu} = p^{\mu}_{\overline{l}} + p^{\mu}_{{l}}$ and the matrix element for $\mathcal{O}_{10}^{(\prime)}$ operator is given by:
\begin{align}
 \braket{\mup\mun|\mathcal{O}^{(\prime)}_{10}|\Bds} &=\mp if_{\Bds}p^\mu\braket{\mup\mun|\overline{l}\gamma_{\mu}\gamma_5l|0} \nonumber \\
 &= \mp if_{\Bds}\left ( \braket{\mup\mun|\overline{l}p^{\mu}_{\overline{l}}\gamma_{\mu}\gamma_5l|0} - \braket{\mup\mun|\overline{l}\gamma_5p^{\mu}_{{l}}\gamma_{\mu}l|0}\right )\nonumber \\
 & = \mp 2if_{\Bds} m_{\mu} \braket{\mup\mun|\overline{l}\gamma_5l|0} = \mp 2if_{\Bds} m_{\mu} \eta_\lambda M_\Bds
\end{align}
\noindent where the anticommutation property of the Dirac matrices is used in the second line, the Dirac equation for particles and antiparticles is applied in the last line, and the expression for the pseudoscalar leptonic matrix element given in Eq.~\ref{eq:scalar-pseudoscalar-leptonic} is also used.

\subsection{\texorpdfstring{\Bmm}{BtoMuMu} decay amplitude}
The contributions from the operators derived above are combined using the Hamiltonian from Eq.~\ref{eq:matrix-element-general} to obtain the matrix element for \Bmm decays for independent helicities, which is given by 

\begin{align}
 \mathcal{M}(\Bds\to\mu^+_\lambda\mu^-_\lambda) = & - i\frac{G_F \alpha}{\sqrt{2}\pi}f_\Bds M_\Bds m_\mu V^*_{tq} V_{tb} \times \nonumber \\
 & \biggl\{ \eta_{\lambda} \left (C_{10} - C_{10}^\prime + \frac{M_\Bds^2}{2m_\mu} \left (\frac{m_b}{m_b+m_q}\right )(C_P - C_P^\prime) \right ) + \nonumber \\
 & \frac{M_\Bds^2}{2m_\mu}\sqrt{1-\frac{4m_\mu^2}{M_\Bds^2}}\left (\frac{m_b}{m_b+m_q}\right ) (C_S - C_S^\prime) \biggr\}
 \label{eq:matrix-element-explicit}
\end{align}
\indent The value of the Wilson coefficients contributing to the \Bmm decays are evaluated for the Standard Model by matching the effective amplitude with the full Standard Model one in the low energy region, $E << M_\W$. From this matching, the only non-negligible contribution to the Standard Model comes from the $C_{10}$ coefficient, given by $C_{10}=-4.134~$\cite{Buras:2013uqa}. The $C_{10}$ term in the decay amplitude is proportional to the muon mass, implying that the decay would be forbidden if muons were massless particles, reflecting helicity suppression already mentioned. The scalar and pseudoscalar contributions do not scale with the muon mass, illustrating that helicity suppression is lifted in these beyond the Standard Model cases. Although technically the Higgs penguin can contribute to the \Bmm process in the Standard Model, its contribution is highly suppressed due to the Higgs coupling to the muons being proportional to $m_\mu/m_\W \approx 10^{-3}$. Non-perturbative QCD effects are fully enclosed in the decay constant, $f_\Bds$, and are calculated to high precision using lattice QCD~\cite{Bazavov:2017lyh}, resulting in a clean computation of the \Bmm amplitudes in the Standard Model. 

Due to the sensitivity of the \Bmm process to New Physics involving scalar, pseudoscalar and axial vector contributions, it is common to express the \Bmm decay amplitude relative to the Standard Model $C_{10}^{SM}$contribution:
\begin{equation}
 \mathcal{M}(\Bmm) = - i\frac{G_F \alpha}{\sqrt{2}\pi}\mathcal{C}_{10}^{SM}f_\Bds M_\Bds m_\mu V^*_{tq} V_{tb} [ \eta_\lambda P +S ]
 \label{eq:matrix-element-sm}
\end{equation}
\noindent where 
\begin{align}
 P = & \frac{C_{10} - C_{10}^\prime}{C_{10}^{SM}} + \frac{M_\Bds^2}{2m_\mu} \left (\frac{m_b}{m_b+m_q}\right )\frac{C_P - C_P^\prime}{C_{10}^{SM}} \nonumber \\
 S = & \frac{M_\Bds^2}{2m_\mu}\sqrt{1-\frac{4m_\mu^2}{M_\Bds^2}}\left (\frac{m_b}{m_b+m_q}\right ) \frac{C_S - C_S^\prime}{C_{10}^{SM}} 
 \label{eq:p-s}
\end{align}
\noindent with $\mathcal{C}_{10} = \mathcal{C}_{10}^{SM}$ in the Standard Model such that $P=1$ and $S=0$.

\section{Radiative corrections}
\label{sec:radiative-corrections}
Photon emission takes place in the \Bmm process. Two types of emission can be distinguished depending on the particles emitting the photons. \textit{Final-state radiation} is emitted from the final-state muons due to bremsstrahlung, from interactions with the detector material or from the higher order QED corrections to the Feynman diagram. If photons are emitted by the initial-state quarks or the decay mediators, such as the \W or \Z bosons, the photon emission is known as \textit{initial-state radiation}. Although interference between the two types is possible, it is found to be negligible for \Bmm decays~\cite{Buras:2012ru}. The magnitude of the interference contribution is visible in Fig.~\ref{fig:isr-fsr}, which shows the breakup of the total \BsmmGamma spectrum in its components~\cite{Dettori:2016zff}. Two separate regions can be distinguished in Fig.~\ref{fig:isr-fsr} based on the dominant radiation contribution. Due to this clear separation, the two emission types are treated separately in the \Bmm analysis.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=\textwidth]{images/Theory/fsr-isr}
 \caption{Breakup of the mass spectrum for \BsmmGamma decays with the different components: pure ISR (long-dashed line), pure FRS (medium-dashed line), interference between ISR and FSR (dot-dashed line). The $y$-axis corresponds to the differential branching fraction scaled by the total branching fraction. The full spectrum is given in solid blue. Adapted from Ref.~\cite{Dettori:2016zff}.}
 \label{fig:isr-fsr}
\end{figure}

\subsection{Initial-State Radiation (ISR)}
Initial-state radiation is dominated by hard photons with relatively large momenta, becoming the dominant photon emission at low dimuon invariant mass. When the photon is emitted by the \Bds meson, the helicity suppression is lifted, and the \BmmGamma decay is sensitive to different Wilson coefficients than the \Bmm decay, such as the $C_7$ from the QED penguin contributions. Therefore, the initial-state radiation should not be included in the \Bmm branching fraction as it de facto is a different decay process. In this analysis, the \BsmmGamma decay involving only initial-state radiation is treated as an independent signal component in the dimuon invariant mass spectrum, and its branching fraction assuming $\mmumu > 4.9 \gevcc$ is also measured, together with the \Bsmm and \Bdmm branching fraction. 

\subsection{Final-State Radiation (FSR)}
Photons emitted by the final-state muons are generally soft photons. Consequently, final-state radiation is the primary source of photon emission in the high invariant mass region, closer to the \Bmm peaks. The FSR transition is theoretically similar to the \Bmm process in so far that they are sensitive to the same Wilson coefficients and with the same decay amplitude. Hence, FSR is an integral part of the \Bmm processes and is included in the measurement of their branching fractions. 

From the theoretical approach, the SM prediction of the \Bmm branching fraction is evaluated without including such final-state radiative contributions~\cite{Buras:2012ru}. The radiative branching fraction, including final-state radiation with total energy $\Delta E$, can be determined as~\cite{Buras:2012ru,Beneke:2019slt}:
\begin{equation}
 \mathcal{B}^{phys}(\Delta E) = \mathcal{B}^0\times \Omega(\Delta E)
\end{equation}
\noindent where $\mathcal{B}^0$ is the non-radiative branching fraction and $\Omega(\Delta E)$ is the radiative factor~\cite{Beneke:2019slt} given by
\begin{equation}
\Omega(\Delta E) = \left ( \frac{2\Delta E}{M_{\Bds}} \right )^{-\frac{2\alpha_{em}}{\pi}\left (1 + \ln{\frac{m_\mu^2}{M^2_\Bds}}\right )}
\end{equation}
\noindent with $\alpha_{em}$ as the electromagnetic coupling constant, and $\Delta E$ giving the experimental signal region around the \Bds mass peak in terms of the dimuon invariant mass. The fully inclusive FSR branching fraction has been found to be in agreement with the non-radiative value up to a 1\% correction~\cite{Buras:2012ru,Beneke:2019slt}. 

From the experimental side, final-state radiation is included in simulation using a dedicated software known as \photos~\cite{Golonka:2005pn} (see Chapter~\ref{ch:lhcb-detector}). The branching fraction is measured by performing a fit over a specific invariant mass window. The FSR effect is included within the efficiency correction, in particular through the selection efficiency, which includes the requirement for the specific mass window. The selection efficiency is evaluated from the simulation samples. Hence, the measured branching fraction corresponds to the fully inclusive radiative branching fraction.

\section{Theory versus experiment}
\label{sec:theory-vs-experiment}
The branching fraction is one of the observables used to probe the Standard Model and search for New Physics using \Bmm decays. It gives the fraction of \Bds mesons decaying into two oppositely charged muons relative to the total number of decaying \Bds mesons. Although the definition holds for theory and experiment, a few differences in their evaluation must be considered when comparing the experimental and theoretical branching fraction evaluations, especially in the case of the \Bsmm decays. 

In the experiment, the \Bds and \Bdsb mesons can be distinguished using the so-called \textit{flavour-tagging} technique. Because the efficiencies associated with this technique are low and due to the rareness of the \Bmm process, the \Bmm analysis does not use flavour-tagging in order to include all the \Bsmm candidates in the analysis. Instead, no distinction is made between \Bds and \Bdsb initial states such that the \textit{untagged decay rate} is defined as:
\begin{equation}
 \braket{\Gamma(\Bds(t)\to\mup\mun)} = \Gamma(\Bds(t)\to\mup\mun) + \Gamma(\Bdsb(t)\to\mup\mun)
\label{eq:untagged-decay-rate}
\end{equation}

\subsection{\texorpdfstring{\Bds}{B} and \texorpdfstring{\Bdsb}{Bbar} mixing}
The mixing between the \Bds and \Bdsb states is a particular characteristic of the \Bds meson system that must be considered when evaluating their time evolution. This mixing arises from the existence of two different eigenstate types for a hadronic particle: the flavour and the mass eigenstates. Flavour eigenstates have a well-defined quark content and are eigenstates of the strong and electromagnetic interactions. Nevertheless, the mesons propagate on the mass eigenstate basis, with well-defined lifetimes and masses. In the case of the \Bds meson, the \Bds and \Bdsb states are flavour eigenstates, while the light and heavy mass eigenstates, \Bhds and \Blds, are a linear combination of the flavour eigenstates. Therefore, the untagged decay rate can be written in terms of the heavy and light mass eigenstates as 
\begin{align}
 \braket{\Gamma(\Bds(t)\to\mup\mun)} =& \Gamma(\Bhds\to\mup\mun)e^{-\Gamma_Ht} + \Gamma(\Blds\to\mup\mun)e^{-\Gamma_Lt} \nonumber \\
 = &(\Gamma(\Bhds\to\mup\mun) + \Gamma(\Blds\to\mup\mun)) \nonumber \\
 &\times e^{-t/\tau_\Bds} ({\cosh(y_s t/\tau_\Bds) + \ADeltaGamma \sinh(y_s t/\tau_\Bds)}) \nonumber \\
 = & (\Gamma(\Bds\to\mup\mun) + \Gamma(\Bdsb\to\mup\mun)) \nonumber \\
 &\times e^{-t/\tau_\Bds} ({\cosh(y_s t/\tau_\Bds) + \ADeltaGamma \sinh(y_s t/\tau_\Bds)}) 
 \label{eq:untagged-decay-rate-time}
\end{align}
\noindent where $\tau_\Bds$ is defined as the inverse of the average decay rate of the light and heavy mass eigenstates. Two new variables appear in the time-dependent factors from the untagged decay rate. The first one, $y_{d(s)}$, is the decay time asymmetry between the mass eigenstates, a property of the \Bs meson system which is independent of the final state:
\begin{equation}
 y_{d(s)} = \frac{\Gamma_{d(s),L} - \Gamma_{d(s),H} }{\Gamma_{d(s),L} + \Gamma_{d(s),H} } 
 \label{eq:ys-def}
\end{equation}
\noindent where $\Gamma_{d(s),L}$ and $\Gamma_{d(s),H}$ are the decay rates of the light and heavy mass eigenstates. The second term, \ADeltaGamma, is known as the decay rate asymmetry and is dependent on the specific decay process:
\begin{equation}
 \mathcal{A}^{\mup\mun}_{\Delta\Gamma} = \frac{\Gamma(\Bhds\to\mup\mun) - \Gamma(\Blds\to\mup\mun)}{\Gamma(\Bhds\to\mup\mun) + \Gamma(\Blds\to\mup\mun)} 
 \label{eq:adeltagamma-def}
\end{equation}
\noindent where $\Gamma(\Bhds\to\mup\mun)$ and $\Gamma(\Blds\to\mup\mun)$ are the decay rates to two muons of the heavy and light mass eigenstates~\cite{DeBruyn:2012wj}. From its definition, the values of the decay rate asymmetry range from +1 to -1 depending on the contribution of each mass eigenstate to the two muons final-state.

The final ingredient in the evaluation of the untagged decay rate is the instantaneous decay rate for the $\Bds\to\mup\mun$ and $\Bdsb\to\mup\mun$ processes. These are obtained using Fermi's Golden rule~\cite{Halzen:1984mc} summing over helicity states of the final-state muons as:
\begin{equation}
 \Gamma(\Bmm)\Bigr |_{t=0} = \frac{1}{16\pi}\frac{1}{M_\Bds}\sqrt{1-\frac{4m_{\mu}^2}{M_{\Bds}^2}}\sum_{\lambda = R,L}|\mathcal{M}(\Bds(t)\to\mu^+_\lambda\mu^-_\lambda)|^2
 \label{eq:fermis-rule}
\end{equation} 
\noindent where $\mathcal{M}(\Bds(t)\to\mu^+_\lambda\mu^-_\lambda)$ is the amplitude of the \Bmm process for individual helicities given in Eq.~\ref{eq:matrix-element-sm}. The decay amplitude for the $\overline{B}_{(s)}^{0}(t)\to\mup\mun$ is obtained analogously. Plugging Eq.~\ref{eq:fermis-rule} into Eq.~\ref{eq:untagged-decay-rate-time}, the untagged decay rates takes the form:
\begin{align}
 \braket{\Gamma(\Bds(t)\to\mup\mun)} = & \frac{G_F^2\alpha^2}{8\pi^3}|C_{10}^{SM}V_{t(d,s)}V_{tb}^*|^2 f_\Bds^2 M_\Bds m^2_\mu \sqrt{1-\frac{4\m^2_\mu}{M_\Bds^2}}(|P|^2 + |S|^2) \nonumber \\
 &\times e^{-t/\tau_\Bds} ({\cosh(y_s t/\tau_\Bds) + \ADeltaGamma \sinh(y_s t/\tau_\Bds)})
 \label{eq:full-untagged-decay-rate}
\end{align}
\noindent where $P$ and $S$ are given by Eq.~\ref{eq:p-s} and are defined such that in the Standard Model $P = 1$ and $S = 0$.

\subsection{Relation between experimental and theoretical definitions}

The theoretical branching fraction is calculated from the CP-average decay rate of the flavour eigenstates, \Bds and \Bdsb. It is evaluated assuming $t=0$ in Eq.~\ref{eq:full-untagged-decay-rate}, such that it is not affected by the \Bds-\Bdsb mixing, only by the \Bds mean lifetime~\cite{DeBruyn:2012wj}
\begin{align}
 \mathcal{B}_{th}(\Bmm) = & \frac{\tau_{\Bds}}{2}\braket{\Gamma(\Bds(t)\to\mup\mun)}\Bigr |_{t=0} \\
 = & \frac{\tau_\Bds G_F^2\alpha^2}{16\pi^3}|C_{10}^{SM}V_{t(d,s)}V_{tb}^*|^2 f_\Bds^2 M_\Bds m^2_\mu \sqrt{1-\frac{4\m^2_\mu}{M_\Bds^2}}(|P|^2 + |S|^2) 
 \label{eq:th-bf}
\end{align}
\indent Experimentally, the branching fraction is measured from the total detected \Bmm candidates generated in the proton-proton collision, regardless of their decay time. Since the information on the lifetime of the particle is ignored, the experimental branching fraction is defined as~\cite{DeBruyn:2012wj}
\begin{equation}
\mathcal{B}_{exp}(\Bmm) = \frac{1}{2}\int_{0}^\infty\braket{\Gamma(\Bds(t)\to\mup\mun)}dt
\label{eq:exp-bf}
\end{equation}
\indent Using the untagged decay rate computed in the previous subsection, given in Eq.~\ref{eq:full-untagged-decay-rate}, and performing the integral, the experimental branching fraction is found to be related to the theoretical one as~\cite{DeBruyn:2012wj}:
\begin{equation}
 \mathcal{B}_{exp}(\Bmm) = \left [ \frac{1+\mathcal{A}^{\mup\mun}_{\Delta\Gamma}y_{d(s)}}{1-y_{d(s)}^2} \right ]\mathcal{B}_{th}(\Bmm)
 \label{eq:exp-vs-th}
\end{equation}

\subsection{Branching fraction predictions}
The relation between the definitions of experimental and theoretical branching fractions depends on the decay time asymmetry and decay rate asymmetry parameters. In the case of the \Bd meson, the decay rates of the heavy and light mass eigenstates are approximately the same, resulting in a decay time asymmetry of zero: $y_{d} = 0$. Consequently, the experimental and theoretical branching fraction definitions are equal, as given from Eq.~\ref{eq:exp-vs-th}. The \Bdmm branching fraction as predicted in the Standard Model is~\cite{Beneke:2019slt} 
\begin{equation}
 \mathcal{B}(\Bdmm) = (1.03\pm 0.05)\times10^{-10}
\end{equation}
\indent The case for the \Bs meson system is more complex. Because the decay time asymmetry has been measured to be $y_s = 0.068\pm0.004$~\cite{PDG}, the role of the \ADeltaGamma becomes significant in Eq.~\ref{eq:exp-vs-th}. As shown in Eq.~\ref{eq:adeltagamma-def}, the \ADeltaGamma parameter quantifies the decay rate asymmetry of the heavy and light mass eigenstates. Due to angular momentum conservation, the final state of the \Bmm process is CP-odd since the two final-state muons have opposite spins and zero total angular momentum. Regarding the \Bs meson system, the heavy (light) mass eigenstate is also identified as the CP-odd (CP-even) eigenstate. In the Standard Model, no CP violation is allowed in this decay, meaning that only the heavy mass eigenstate decays into two muons and \ADeltaGamma = +1. Taking this into account in Eq.~\ref{eq:exp-vs-th}, the Standard Model prediction for the \Bsmm branching fraction is given by~\cite{Beneke:2019slt}
\begin{gather}
 \mathcal{B}(\Bsmm) = (3.66\pm 0.14)\times10^{-9} \nonumber \\
 \mathcal{B}(\Bdmm) = (1.03\pm 0.05)\times10^{-10}
\end{gather}
\indent Although outside of the scope of this thesis, it is important to highlight that the decay rate asymmetry $\mathcal{A}^{\mup\mun}_{\Delta\Gamma}$ is essentially unknown. If CP violation occurs during the decay, the light mass eigenstate could also decay into two muons, and the decay rate asymmetry would then be different to its Standard Model value of +1. Therefore, $\mathcal{A}^{\mup\mun}_{\Delta\Gamma}$ is also sensitive to New Physics scenarios. Its measurement is possible through the measurement of the \Bsmm effective lifetime, defined as the average decay time of the \Bsmm candidates in the experiment and given by~\cite{DeBruyn:2012wk}
\begin{equation}
 \tauBsmm = \frac{2\tauBs\ADeltaGamma\ys+\left(1+\ys^2\right)\tauBs}{\left(1-\ys^2\right)+\ADeltaGamma\ys \left(1-\ys^2\right)}
 \label{eq:eff-lifetime}
\end{equation}
\indent The effective lifetime has recently been measured by the \lhcb experiment~\cite{LHCb:2021vsc,LHCb:2021awg} and agrees with the Standard Model prediction.

