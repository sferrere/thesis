\newpage
\chapter{Branching fraction measurement}
\label{ch:BF-measurement-results}
This chapter presents the extended maximum likelihood fits to the dimuon invariant mass spectrum of the selected candidates resulting in three branching fraction measurements: \Bsmm, \Bdmm and the initial-state radiation \BsmmGamma. The concepts of likelihood and maximum likelihood estimator are first introduced in Sec.~\ref{sec:maximum-likelihood-estimators}, while the fit results are presented in Sec.~\ref{sec:bf-ml-fit}. For each signal channel, the significance of the branching fraction measurement determined from the fit is obtained following the approach presented in Sec.~\ref{sec:experimental-significance}. Since the significances associated with the \Bdmm and \BsmmGamma decays are lower than the minimum required to claim a discovery, an upper limit is set to their branching fractions using the CL$_{\text{s}}$ method, described in Sec.~\ref{sec: limit-cls-method}, resulting in the upper limits given in Sec.~\ref{sec:significance-bmmgamma}. 

\section{Maximum likelihood estimators}
\label{sec:maximum-likelihood-estimators}
Consider an hypothesis $\mathcal{H}$ described by a set of model parameters $\boldsymbol{\theta} = \{\theta_j,...\theta_{m}\}$ and a set of measurements $\textbf{x}=\{x_i,...x_n\}$ distributed according a probability density function (p.d.f.) $f(\textbf{x}|\boldsymbol{\theta})$. Assuming $\mathcal{H}$, the \textit{likelihood function} represents how well the parameters $\boldsymbol{\theta}$ describe the observed measurements $\textbf{x}$. Supposing that the measurements are independent and described by the same probability density function $f(x_i|\boldsymbol{\theta})$, the likelihood function is defined as the product of the probability density functions of every single measurement ~\cite{Cowan:1998ji, PDG}:
\begin{equation}
 L(\boldsymbol{\theta}|\textbf{x}) = \prod_{i=1}^{n}f(x_i|\boldsymbol{\theta})
 \label{eq: def-likelihood}
\end{equation}
Generally, one deals with \textit{composite hypotheses}, where the shape of the p.d.f. $f(x_i|\boldsymbol{\theta})$ is known but not the value of all the $\boldsymbol{\theta}$ parameters describing it~\cite{Cowan:1998ji}. In these cases, the values of the unknown parameters can be estimated given the observed data using the \textit{maximum likelihood estimator\footnote{An estimator is a function to evaluate a quantity or parameter of the p.d.f as a function of the observed data.~\cite{Cowan:1998ji}.} (MLE)} method. From the definition of the likelihood, it is clear that the higher its value, the more probable the composite hypothesis is correct. Keeping this in mind, the $\boldsymbol{\theta}$ parameters that best describe the data are given by:
\begin{equation}
 \frac{\partial L(\boldsymbol{\theta})}{\partial \theta_j} = 0\text{, where }j = 1,..., m
\end{equation}
From the construction of the likelihood as a product of probability density functions, it is mathematically more convenient to maximize $\ln{L(\boldsymbol{\theta})}$ instead of the likelihood function itself. The use of the logarithm allows for each candidate to be a separate term in the $\ln{L(\boldsymbol{\theta})}$ as well as reducing the large numerical values associated with the $L(\boldsymbol{\theta})$. Using $\ln{L(\boldsymbol{\theta})}$, the maximum likelihood estimators give the parameter of the p.d.f. best describing the data as
\begin{equation}
 \frac{\partial \ln(L(\boldsymbol{\theta}))}{\partial \theta_j} = 0\text{, where }j = 1,..., m
 \label{eq:mle-log}
\end{equation}
The number of observed measurements $n$, which in this analysis corresponds to the number of events, follows a Poisson distribution described by the average expected event rate, $\upsilon$. To include this realization, the likelihood is smeared by adding the corresponding Poisson probability density function as an overall normalization of the likelihood function in Eq.~\ref{eq: def-likelihood}. This addition results in the so-called \textit{extended likelihood function}~\cite{Cowan:1998ji} 
\begin{equation}
 L(\upsilon, \boldsymbol{\theta}) = \frac{\upsilon^{n}}{n!}e^{-\upsilon}\prod_{i=1}^{n}f(x_i|\boldsymbol{\theta})
\end{equation}
\noindent where $f(x_i|\boldsymbol{\theta})$ are the p.d.f. of each independent measurement, $n$ is the number of measurements and $\upsilon$ the estimated true yield, affecting the normalization of the likelihood function. 

It is often the case that only a few of the $\boldsymbol{\theta}$ parameters describing the hypothesis $\mathcal{H}$ are of interest in an analysis, while the rest of the parameters, known as \textit{nuisance parameters}, are needed to define the likelihood function properly~\cite{Cowan:1998ji}. Although not of intrinsic interest, the correct estimation of the nuisance parameters is essential as they might bias the predictions of the parameters of interest. Typically, the nuisance parameter estimation is done using different methods, from calibration samples to the use of control channels, or simply fitted in the final fit. A summary of the parameters describing the likelihood function used in the \BmmGamma analysis and the calibration methods followed in their estimation can be found in Table~\ref{tab:tab-parameters-fit}.

\input{chapters/tabs/bf-measurement-results/tab-parameters-fit.tex}

\section{\texorpdfstring{\BmmGamma}{BMuMuGamma} branching fraction ML fit}
\label{sec:bf-ml-fit}
The branching fractions of the \BmmGamma decays are obtained from unbinned extended maximum likelihood fits to the dimuon invariant mass spectrum of the selected candidates in the data samples. The dataset comprises the data gathered by LHCb during the Run 1 and Run 2 data-taking periods. In order to maximize the sensitivity of the analysis, the entire dataset is divided into two subsamples for each data-taking period, Run 1 and Run 2, which are further split into six bins of the BDT classifier. As mentioned in Sec.~\ref{sec:BDT-concept}, the BDT binning used is optimized to maximize the signal sensitivity while keeping the background manageable. The fits are performed simultaneously in all the data-taking periods and BDT bin subsets. The difference in luminosity for each data-taking period is accounted for by weighting each data-taking subset with the correct luminosity fraction. A similar procedure is followed for each BDT bin subset using the BDT calibration results in Chapter~\ref{ch:bdt-calibration}. The first BDT bin, corresponding to \bdtone, is not included in the fit because the combinatorial background dominates it. However, the fraction of events expected in this first BDT bin is included in the normalization factor as an efficiency correction, as described in Sec.~\ref{sec:normalization}.

The signal and background components are modelled independently in the fit using different probability density functions. The \Bdmm and \Bsmm components are modelled with two independent double-sided Crystal Ball functions (see Appendix~\ref{sec:dscb}) whose characterizing parameters are allowed to vary following a Gaussian with a specific mean and width defined by the mass calibration procedure in Sec.~\ref{sec:mass-calibration}. The \BsmmGamma component only includes initial-state radiation (see Chapter~\ref{ch:theory}) and it is modelled using an empirical function given by
\begin{equation}
 f(\mmumu) \propto \big(1-\frac{\mmumu}{M_{\Bs}}\big)^b - a\sqrt{1-e^{\frac{\mmumu - M_{\Bs}}{s}}}
 \label{eq: shape-for-bsmmgamma}
\end{equation}
\noindent where the parameters $b$, $a$ and $s$ are determined from fits to dedicated simulation samples obtained based on the theoretical calculations from Refs.~\cite{Melikhov:2004mk,Kozachuk:2017mdk}. The $b$ parameter is found to be 0.5, while different values of the $a$ and $s$ parameters are obtained for different BDT bins. The detector resolution effects are included by convolving the empirical shape in Eq.~\ref{eq: shape-for-bsmmgamma} with a Gaussian function whose parameters are obtained from weighted simulation samples. All the parameters describing the \BsmmGamma mass component are fixed in the fit to the data except the yield. The other two nuisance parameters associated with the signal channels, namely the normalization factors (see Sec.~\ref{sec:normalization}) and the expected fractions of signal events ending up in each BDT bin (see Chapter~\ref{ch:bdt-calibration}), are also Gaussian constrained in the fit.

Regarding the backgrounds, the combinatorial component is described with an exponential function whose slope is left free in the fit and shared among all the data-taking and BDT subsets. The shapes of the physical backgrounds are obtained from simulation samples using Gaussian kernel density estimators~\cite{Cranmer:2000du}. While the yields of the combinatorial background are left free in the fit, the yields of each physical background component are Gaussian constrained to the expected yields evaluated in Sec.~\ref{sec:peaking-backgrounds}. 

\begin{figure}[htbp!]
 \centering
 \includegraphics[width=0.495\textwidth]{images/BF-results/fits/fit_run1_bdt1.pdf}
 \includegraphics[width=0.495\textwidth]{images/BF-results/fits/fit_run1_bdt2.pdf}
 \includegraphics[width=0.495\textwidth]{images/BF-results/fits/fit_run1_bdt3.pdf}
 \includegraphics[width=0.495\textwidth]{images/BF-results/fits/fit_run1_bdt4.pdf}
 \includegraphics[width=0.495\textwidth]{images/BF-results/fits/fit_run1_bdt5.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/legend.pdf}
 \caption{Mass distributions of the \BmmGamma selected candidates for Run 1 for each different BDT bin. The blue line corresponds to the total fit model, and the separated components are reported in the legend. Solid bands are shown for the distributions of the signal channels indicating the total uncertainties.}
 \label{fig:ml-bf-fit-r1}
\end{figure}
\begin{figure}[htbp!]
 \centering
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/fit_run2_bdt1.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/fit_run2_bdt2.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/fit_run2_bdt3.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/fit_run2_bdt4.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/fit_run2_bdt5.pdf}
 \includegraphics[width=0.49\textwidth]{images/BF-results/fits/legend.pdf}
 \caption{Mass distributions of the \BmmGamma selected candidates for Run 2 for each different BDT bin. The blue line corresponds to the total fit model, and the separated components are reported in the legend. Solid bands are shown for the distributions of the signal channels indicating the total uncertainties.}
 \label{fig:ml-bf-fit-r2}
\end{figure}

The mass distributions of the signal candidates are shown in Fig.~\ref{fig:ml-bf-fit-r1} and Fig.~\ref{fig:ml-bf-fit-r2} for Run 1 and Run 2 and the corresponding BDT bins. The total fit and its separated components are also displayed. The branching fractions for the signal channels are obtained from these fits and found to be
\begin{align*}
 \mathcal{B}(\Bsmm) &= (3.09^{+0.46+0.15}_{-0.43-0.11})\times 10^{-9}\\
 \mathcal{B}(\Bdmm) &= (1.02^{+0.83}_{-0.74}\pm 0.14)\times 10^{-10}\\
 \mathcal{B}(\BsmmGamma; \mmumu \geq &4.9 \gevcc) = (-2.5\pm 1.4 \pm 0.8)\times 10^{-9}
\end{align*}
\noindent where the statistical and systematic uncertainties are reported separately. It is important to note that the \BsmmGamma branching fraction only includes initial-state radiation in the dimuon invariant mass window given by $m_{\mup\mun} \geq 4900 \gevc $. The negative result obtained is compatible with the very low sensitivity for that decay and suggests an under-fluctuation of the background on the low dimuon invariant mass region. As previously mentioned, both the statistical and systematic uncertainties of each nuisance parameter are taken into account by Gaussian constraining their values to the range given by their uncertainties. As a consequence, the fit only returns the total uncertainty without isolating the statistical and systematic uncertainty associated with each branching fraction value. To separate the statistical component, the fit is repeated by fixing all the nuisance parameters to the values obtained from the nominal fit. The systematic uncertainties are then extracted by subtracting in quadrature the statistical uncertainties from the total ones in the nominal fit. The primary sources of systematic uncertainty in the branching fractions measurement arise from the \fsfd ratio and the estimation of the exclusive backgrounds yields, mainly the misidentified \BTohh decays due to their significant overlap with the \Bdmm component. 

\section{Significance of a measurement}
\label{sec:experimental-significance}
When particle physicists search for new phenomena, such as the \BsmmGamma or the \Bdmm processes, two hypotheses are tested and compared: the hypothesis assuming that the data is consistent with the presence of only background, also known as the null hypothesis or $H_0$; and the hypothesis including both signal and background, referred to as $H_1$. The possible rejection of the null hypothesis in favour of the $H_1$ is done by measuring the discrepancy between the observed data and the null hypothesis with a \textit{test statistic}\footnote{A test statistic is a quantity derived from data as a function of the measured variables that, together with its value for a given hypothesis, quantifies the compatibility of the observed data and the hypothesis tested.}. The significance of such discrepancy is quantified by the \textit{p-value} of $H_0$, giving the probability that the data obtained under the $H_0$ hypothesis is equally or less compatible than the observed data~\cite{Cowan:1998ji}. Considering a null hypothesis consisting of $\boldsymbol{\theta}$ parameters of interest and $\boldsymbol{\nu}$ nuisance parameters, the one-sided \textit{p}-value for the parameters of interest $\boldsymbol{\theta}$ is~\cite{PDG}
\begin{equation}
 p_{\boldsymbol{\theta}}(\boldsymbol{\nu}) = \int_{q_{\boldsymbol{\theta},obs}}^{\infty} f(q_{\boldsymbol{\theta}}|\boldsymbol{\theta}, \boldsymbol{\nu} ) dq_{\boldsymbol{\theta}}
 \label{eq:p-value-nuisance}
\end{equation}
\noindent where $f(q_{\boldsymbol{\theta}}|\boldsymbol{\theta}, \boldsymbol{\nu} )$ is the probability density distribution of a test statistic $q_{\boldsymbol{\theta}}$ and $q_{\boldsymbol{\theta},obs}$ is the value of the test statistic observed in the data. 

The $H_0$ hypothesis is rejected if the \textit{p}-value is less than $\alpha$, which is the probability of rejecting the null hypothesis when it is true, also known as a \textit{type-I error}~\cite{Cowan:1998ji}. Because the \textit{p}-value in Eq.~\ref{eq:p-value-nuisance} depends on the nuisance parameters, the $H_0$ can only be rejected if the \textit{p}-value is smaller than $\alpha$ for all the possible values of the nuisance parameters.

To remove this dependence of the \textit{p}-value on the nuisance parameters, the test statistic $q_{\boldsymbol{\theta}}$ can be defined such that the p.d.f. $f(q_{\boldsymbol{\theta}}|\boldsymbol{\theta}, \boldsymbol{\nu} )$ is independent of the nuisance parameters, i.e. $f(q_{\boldsymbol{\theta}}|\boldsymbol{\theta})$. One method to do so, used in this analysis, is to profile out the nuisance parameters and express them as a function of the parameters of interest to define the \textit{profile likelihood} function~\cite{PDG} as
\begin{equation}
 L_p(\boldsymbol{{\theta}}) = L(\boldsymbol{\theta}, \boldsymbol{\hat{\nu}}(\boldsymbol{{\theta}}))
\end{equation}
\noindent where $\boldsymbol{\hat{\nu}}(\boldsymbol{{\theta}})$ are the nuisance parameters maximizing the likelihood for a given value of $\boldsymbol{{\theta}}$. A test statistic independent of the nuisance parameters is then the \textit{profile likelihood ratio} defined as
\begin{equation}
 \lambda_p(\boldsymbol{\theta}) = \frac{L_p(\boldsymbol{{\theta}})}{L_p(\boldsymbol{\hat{{\theta}}})}= \frac{L(\boldsymbol{\theta}, \boldsymbol{\hat{\nu}}(\boldsymbol{{\theta}}))}{L(\boldsymbol{\hat{\theta}}, \boldsymbol{\hat{\nu}}(\boldsymbol{\hat{\theta}}))}
 \label{eq:profile-lik-ratio}
\end{equation}
\noindent with $\boldsymbol{\hat{\theta}}$ as the maximum likelihood estimator for the parameters of interest. While the likelihood in the denominator is maximized unconditionally, the likelihood in the numerator is given for the nuisance parameters that maximize the likelihood for a particular value of $\boldsymbol{{\theta}}$, assuming the null hypothesis $H_0$~\cite{PDG, Cowan:2010js}. 

From its definition, the profile likelihood ratio ranges from 0 to 1, with higher values implying higher compatibility of the observed data with the null hypothesis $H_0$, i.e. $\lambda_p(\boldsymbol{\theta})=1$ if $\boldsymbol{\hat{\theta}} = \boldsymbol{{\theta}}$. To sculpt a profile of the likelihood around the maximum, the associated test statistic used is 
\begin{equation}
 t_{\boldsymbol{\theta}} = -2 \ln \lambda_p(\boldsymbol{\theta})
 \label{eq:test-statistic_prof}
\end{equation}
\noindent where higher values of $t_{\boldsymbol{\theta}}$ correspond to higher incompatibility between the observed data and the hypothesized values of $\boldsymbol{\theta}$~\cite{Cowan:2010js}. Notice that $t_{\boldsymbol{\theta}}$ is not dependent on the nuisance parameters since they have been profiled out by expressing them as functions of the parameters of interest. Similarly, the probability density function of the test statistic can be written as $f(t_{\boldsymbol{\theta}}|\boldsymbol{\theta}) = f(t_{\boldsymbol{\theta}}|\boldsymbol{\theta}, \boldsymbol{\hat{\nu}}(\boldsymbol{\theta}))$. As a consequence, the evaluation of the \textit{p-}value using Eq.~\ref{eq:p-value-nuisance} becomes:
\begin{equation}
 p_{\boldsymbol{\theta}} = \int_{t_{\boldsymbol{\theta},obs}}^{\infty} f(t_{\boldsymbol{\theta}}|\boldsymbol{\theta}) dt_{\boldsymbol{\theta}}
 \label{eq:p-value-no-nuisance}
\end{equation} 
Eq.~\ref{eq:p-value-no-nuisance} is defined for a set of parameter of interest $\boldsymbol{\theta}=(\theta_i, ...\theta_m)$. In the \BmmGamma analysis, there are three parameters of interest: the branching fractions of the \Bsmm, \Bdmm and \BsmmGamma signal channels. They are treated independently, such that for any null hypothesis of one of the signal components, the others are treated as nuisance. Therefore, for each null hypothesis tested, there is only one parameter of interest $\boldsymbol{\theta}=\mu$, corresponding to the branching fractions of one signal decay. In these cases, the analytical behaviour of the test statistic in Eq.~\ref{eq:test-statistic_prof} in the large sample limit is known analytically using Wald's theorem~\cite{wald} and can be approximated as~\cite{Cowan:2010js}
\begin{equation}
 t_{\mu} = \frac{(\mu - \hat{\mu})^2}{\sigma^2}
 \label{eq:test-statisic-for-bmm}
\end{equation} 
\noindent where $\hat{\mu}$ and $\sigma$ are the mean and the standard deviations of the estimated branching fraction obtained from the maximum likelihood fit. Moreover, Wilk's theorem states that the distribution of the test statistic $t_{\mu}$ in the large sample limit approaches a \chisq distribution~\cite{Wilks:1938dza}. Therefore, the \textit{p}-values can be adequately defined as the area under the \chisq distribution.

Once the $t_{\mu}$ value is defined for a given hypothesis, the significance of the measurement is determined with the \textit{p}-value using Eq.~\ref{eq:p-value-no-nuisance}. Although the \textit{p}-value is already enough to decide whether to reject the null hypothesis, the significance in particle physics is typically given in terms of the \textit{Z-score}. Interpreting the \textit{p}-value in terms of a normal distribution, the Z-score is obtained as the number of standard deviations between the mean of the Gaussian of the parameter of interest, given by the hypothesis tested, and the measured value~\cite{PDG,Cowan:1998ji}; as shown in Fig.~\ref{fig:z-score}. The Z-score is calculated from the \textit{p}-value using the inverse of the cumulative Gaussian distribution $\Phi^{-1}$ as
\begin{equation}
 Z = \Phi^{-1} (1-p)
\end{equation}
Both the \textit{p}-value and the Z-score are evaluated under the assumption of the null hypothesis (background-only). The level of significance required to claim a discovery is set to a Z-score of five, corresponding to a \textit{p}-value of $2.87\times 10^{-7}$.

\begin{figure}[htbp]
 \centering
 \begin{overpic}[trim={1.45cm 0cm 1.43cm 1cm}, clip, width=0.47\textwidth]{images/BF-results/z_score_figure.pdf}
 \end{overpic} 
 \caption{Example of a Gaussian distribution illustrating the relation between the \textit{p}-value and the Z-score given an observed value of the parameter of interested $\mu$ relative to the value of $\mu$ under the null hypothesis.}
 \label{fig:z-score}
\end{figure}

The significance of the \Bsmm and \Bdmm signals relative to the background-only hypothesis are obtained using the test statistic in Eq.~\ref{eq:test-statisic-for-bmm}. The significance for \Bsmm decays is found to be 10$\sigma$, while for \Bdmm, it is lower than the minimum 5$\sigma$ required to claim a discovery, with a value of 1.7$\sigma$. 

\section{Limit setting with the \texorpdfstring{$\text{CL}_{\text{s}}$}{CLs} technique}
\label{sec: limit-cls-method}
The statistical significance of a particular signal process measurement is sometimes lower than the $5\sigma$ required to claim a discovery. In such cases, an upper limit for the signal branching fraction is reported using \textit{confidence intervals}~\cite{Cowan:1998ji}. Considering the measurement is repeated multiple times, the confidence interval is the range of values the parameter of interest will fall into in a fraction of the experiments given by the \textit{confidence level}. In practice, the experiments are repeated using so-called "Montecarlo toys" or "pseudo"-experiments.

\begin{figure}[htbp]
 \centering
 \begin{overpic}[trim={1.3cm 0cm 1.43cm 1cm},clip, width=0.47\textwidth]{images/BF-results/hypotheses_no_overlap.pdf}
 \end{overpic} 
 \begin{overpic}[trim={1.3cm 0cm 1.43cm 1cm}, clip, width=0.47\textwidth]{images/BF-results/hypotheses_overlap.pdf}
 \end{overpic} 
 %\begin{overpic}[trim={1.45cm 0cm 1.43cm 1cm}, clip, width=0.47\textwidth]{images/BF-results/hypotheses_no_overlap.pdf}
 %\end{overpic} 
 %\begin{overpic}[trim={1.45cm 0cm 1.45cm 1cm}, clip, width=0.47\textwidth]{images/BF-results/hypotheses_overlap.pdf}
 %\end{overpic} 
 \caption{Examples of the $Q$ distributions for the signal+background and the background-only hypothesis when they are well-separated (left) and when they are almost identical (right). The lower the branching fraction tested, the more similar the two distributions are. In the case of the overlap, excluding the hypothesized branching fraction value also implies an exclusion of the background-only hypothesis.}
 \label{fig:hypotheses_situations}
 \end{figure}

A one-sided confidence interval is used to set an upper limit of the branching fraction for the \Bdmm and \BsmmGamma decays. The goal is to find a value of the parameter of interest $\mu$ assuming the signal and background hypothesis such that it is rejected in favour of the background-only hypothesis with a certain confidence level. The confidence level is $1-\alpha$, where $\alpha$ is the type-I error rate or the probability of rejecting the null hypothesis when it is true. The values outside the confidence limit are assumed to be excluded with a $1-\alpha$ confidence level, and the signal+background hypothesis is rejected in those cases. 

The test statistic adopted to set the confidence interval is given by 
\begin{equation}
 Q = -2 \ln{\left ( \frac{L(s+b|\textbf{x})}{L(b|\textbf{x})} \right )} 
 \label{eq:q-test-statistic}
\end{equation}
\noindent where $L(s+b|\textbf{x})$ and $L(b|\textbf{x})$ are the likelihood function for the signal+background and the background-only hypotheses, respectively. This test statistic is more convenient to use in the limit setting since it allows for a direct comparison of the strength of one hypothesis relative to the other. Using this test statistic, the idea is to find a branching fraction value such that the associated \textit{p}-value, obtained using Eq.~\ref{eq:p-value-no-nuisance}, is smaller or equal to $\alpha$. 

In order to find the upper limit, different branching fraction hypotheses are tested. The probability density function of the test statistic for the signal+background hypothesis must be known for each branching fraction value. This probability density function is constructed from a set of simulated pseudo-experiments generated assuming the tested branching fraction value under the signal+background hypothesis. A similar procedure can be followed to construct the probability density function associated with the background-only hypothesis. 

It is important to note that the distributions of the test statistic vary according to the branching fraction hypothesis, as can be deduced from Eq.~\ref{eq:q-test-statistic}, such that the lower the branching fraction tested, the more similar the signal+background and background-only distributions are. To illustrate the consequences of this effect, Fig.~\ref{fig:hypotheses_situations} shows two examples, one where the two distributions are well-separated (left) and the case where they are almost identical (right). In the latter case, excluding a value assuming the signal+background hypothesis will also exclude the background-only hypothesis, leading to an over-exclusion of low branching fraction values for which the experimental sensitivity is not enough.

An alternative to mitigate over-exclusion is the \textit{CL$_\text{s}$ method}~\cite{Read:2002hq,Read:2000ru}. With this approach, an \textit{adjusted} confidence level is obtained, which takes into account the overlap between the two hypotheses
\begin{equation}
 \text{CL}_\text{s} = \frac{p_{s+b}}{1-p_b} 
\end{equation}
\noindent where $p_{s+b}$ and $p_b$ are the \textit{p}-values of the signal+background and background-only hypotheses, respectively. Assuming a specific value of the branching fraction for the signal+background hypothesis, these values are represented in Fig.~\ref{fig:cls_parts} and are evaluated with the test statistic $Q$ as
\begin{align}
 p_{s+b} &= \int_{Q_{\text{obs}}}^{\infty} f_{b+s}(Q)dQ \\
 p_b & = \int_{-\infty}^{Q_{\text{obs}}} f_{b}(Q)dQ
\end{align}
\noindent where $f_{b+s}(Q)$ and $f_{b}(Q)$ are the probability density functions of the signal+background and background-only hypotheses and $Q$ is the test statistic as defined in Eq.~\ref{eq:q-test-statistic}.

\begin{figure}[htbp]
 \centering
 \begin{overpic}[clip, width=0.6\textwidth]{images/BF-results/cls_parts.png} %\begin{overpic}[trim={1.cm 0cm 1.43cm 1cm}, clip, width=0.47\textwidth]{images/BF-results/cls_parts.png}
 \end{overpic} 
 \caption{Illustration showing the $p_b$ and $p_{b+s}$ for the background-only and background+signal hypotheses given a observed value of $Q$.}
 \label{fig:cls_parts}
\end{figure}

The confidence interval is determined using $\text{CL}_\text{s}=\alpha$ by setting the confidence level to $1-\text{CL}_\text{s}$~\cite{Read:2002hq,Read:2000ru}. The probability density function of the $Q$ test statistic is obtained from pseudo-experiments generated using the corresponding hypothesis. 

\section{Limit setting for the \texorpdfstring{\Bdmm}{BdMuMu} and \texorpdfstring{\BsmmGamma}{BsMuMuGamma} branching fractions}
\label{sec:significance-bmmgamma}
Due to the low significance of the \Bdmm and \BsmmGamma decays, an upper limit is set to their branching fractions values using the CL$_{\text{s}}$ method as implemented in the \textsc{GammaCombo} framework~\cite{gammacombo,LHCb:2016mag}. Within this framework, a CL$_{\text{s}}$ scan is performed for different values of the branching fractions to obtain two later compared curves: the expected and the observed curves. The expected or background-only curve is obtained by generating several samples using the background-only model and fitting them back using the signal+background model with the branching fraction fixed at a specific value. The CL$_{\text{s}}$ is then calculated for this fixed value of the branching fractions as described in Sec.~\ref{sec: limit-cls-method}. Since several samples are fitted back with the same branching fraction, not all of them yield the same CL$_{\text{s}}$ value. The 1$\sigma$ (2$\sigma$) error bands associated to the expected curve are then defined as the vertical CL$_{\text{s}}$ intervals where 16\% (2.5\%) of the fitted CL$_{\text{s}}$ fall above and 16\% (2.5\%) below the boundaries. Regarding the observed curve, the CL$_{\text{s}}$ values are determined from fits to the data sample by scanning the likelihood for various values of the branching fraction. 

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.47\textwidth]{images/BF-results/contour_limits/cls_bdmm_limit.pdf}
 \includegraphics[width=0.47\textwidth]{images/BF-results/contour_limits/cls_bsmmgamma_limit.pdf}
 \caption{CL$_\text{s}$ curves for branching fractions of the \Bdmm (left) and \BsmmGamma (right) decays. The observed values in data are shown in black. The expected background-only values are shown in red with the 1$\sigma$ and 2$\sigma$ bands around the values in light and black blue, respectively.}
 \label{fig:brazil-band-plots}
 \end{figure}
 
The resulting CL$_{\text{s}}$ curves for \Bdmm and \BsmmGamma decays are shown in Fig.~\ref{fig:brazil-band-plots} from which the limits on their branching fractions are obtained as
\begin{gather*}
 \mathcal{B}(\Bdmm) < 2.3(2.6)\times 10^{-10}\text{ at 90\%(95\%) CL}\\
 \mathcal{B}(\BsmmGamma) |_{\mmumu \geq 4.9 \gevcc} < 1.5(2.0)\times 10^{-9}\mbox{ at 90\%(95\%) CL}
\end{gather*}
The results obtained for the \Bmm branching fractions are in agreement with the Standard Model predictions of $\mathcal{B}(\Bsmm)= (3.66 \pm 0.14) \times 10^{-9}$ and $\mathcal{B}(\Bdmm) = (1.03 \pm 0.05) \times 10^{-10}$~\cite{Beneke:2019slt}. This agreement is also visible in the two-dimensional confidence interval set for $\mathcal{B}(\Bsmm)$ and $\mathcal{B}(\Bdmm)$ in Fig.~\ref{fig:2d-contours}, where the Standard Model prediction is also represented together with the contours corresponding to the previous LHCb measurement~\cite{LHCb:2017rmj}. The two-dimensional contours obtained for $\mathcal{B}(\BsmmGamma)$ versus $\mathcal{B}(\Bdmm)$ and $\mathcal{B}(\Bsmm)$ are also presented in Fig.~\ref{fig:2d-contours}.  

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.47\textwidth]{images/BF-results/contour_limits/contour_bdmm_bsmm.pdf}\\
 \includegraphics[width=0.47\textwidth]{images/BF-results/contour_limits/contour_bsmmgamma_bdmm.pdf}
 \includegraphics[width=0.47\textwidth]{images/BF-results/contour_limits/contour_bsmmgamma_bsmm.pdf}
 \caption{Two-dimensional confidence intervals of the branching fractions for the \Bsmm and \Bdmm decays (top) where the Standard Model predictions are shown in red, and the contours in brown indicate the results from the previous LHCb measurement~\cite{LHCb:2017rmj}. The two-dimensional counters are also shown for the \BsmmGamma and \Bdmm decays (bottom left) and \BsmmGamma and \Bsmm decays (bottom right).}
 \label{fig:2d-contours}
\end{figure}

