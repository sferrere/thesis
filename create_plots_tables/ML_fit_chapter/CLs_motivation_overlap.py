import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math

color_alpha = "green"
color_norm_dist1 = "dimgrey"
color_norm_dist2 = "blue"
color_observed = "navy"

x = np.arange(-25, 25, 0.1)


# Initialize plots
fig, ax = plt.subplots()
# Plot normal distribution for background + signal
mu1 =16-10
variance1 = 22
sigma1 = math.sqrt(variance1)
plt.plot(
	x, stats.norm.pdf(x, mu1, sigma1), 
	color = color_norm_dist2, 
	label = "Signal+Background",
	ls = "-."
)

# Plot normal distribution for background
mu = 29-20
variance = 12
sigma = math.sqrt(variance)
plt.plot(
	x, stats.norm.pdf(x, mu, sigma), #chi2.pdf(x, df=8), #
	color = color_norm_dist1,
	label = "Background-only",
)


# Set axis style
plt.axis(
	ymin= 0.0, 
	xmin = -25.0, 
	xmax = 25,
	#ymax = 0.45
)

plt.tick_params(
    axis='both',         
    which='both', 
    #direction="in",
    #bottom=False, 
    left=False,
    #left=False,
    #labelbottom=False, 
    labelleft=False,
    labelsize = 10

 )
ax.set_xlabel(
	'$Q$', 
	loc='right', 
	size = 15
)
ax.set_ylabel(
	'$f(Q)$', 
	loc='top', 
	size = 15
	)
plt.axis(
	ymin= 0.0, 
	xmin = -25.0, 
	xmax = 25,
	#ymax = 0.45
)


# Observed value line
plt.vlines( #-3
	x=3.5, ymin = 0, ymax = 0.10, 
	ls=':', 
	color = color_alpha, 
	label = "Observed $Q$ value"
)

# Set alpha area
"""
x_fill = np.arange(-20, 3, 0.1)
y_fill = stats.norm.pdf(x_fill, mu, sigma)
plt.fill_between(
	x_fill,
	y_fill,
	0, 
	color=color_alpha,
	alpha=0.5,
	label = r'$ \alpha $ = 0.05'
)
"""


plt.legend(
	loc='upper left', 
	frameon=False,
	prop={'size': 13}

)

#plt.show()

plt.savefig("overlap.pdf")

plt.close()
