import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math

color_clsb = "green"
color_clb = "orange"
color_norm_dist1 = "dimgrey"
color_norm_dist2 = "blue"
color_observed = "navy"

x = np.arange(-15, 25, 0.1)


# Initialize plots
fig, ax = plt.subplots()
# Plot normal distribution for background + signal
mu1 =16-10
variance1 = 22
sigma1 = math.sqrt(variance1)
plt.plot(
	x, stats.norm.pdf(x, mu1, sigma1), 
	color = color_norm_dist2, 
	label = "Signal+Background",
	ls = "-."
)

# Plot normal distribution for background
mu = 9 
variance = 12
sigma = math.sqrt(variance)
plt.plot(
	x, stats.norm.pdf(x, mu, sigma), 
	color = color_norm_dist1,
	label = "Background-only",
)


# Set axis style
plt.axis(
	ymin= 0.0, 
	xmin = -15.0, 
	xmax = 20,
	#ymax = 0.45
)

plt.tick_params(
    axis='both',         
    which='both', 
    #direction="in",
    #bottom=False, 
    left=False,
    #left=False,
    #labelbottom=False, 
    labelleft=False,
    labelsize = 10

 )
ax.set_xlabel(
	'$Q$', 
	loc='right', 
	size = 15
)
ax.set_ylabel(
	'$f(Q)$', 
	loc='top', 
	size = 15
	)
plt.axis(
	ymin= 0.0, 
	xmin = -15.0, 
	xmax = 20,
	#ymax = 0.45
)


# Observed value line
plt.vlines( #-3
	x=4, ymin = 0, ymax = 0.10, 
	ls=':', 
	color = color_observed, 
	label = "Observed $Q$ value"
)

# Set CL_sb area
x_fill = np.arange(4, 40, 0.1)
y_fill = stats.norm.pdf(x_fill, mu1, sigma1)
plt.fill_between(
	x_fill,
	y_fill,
	0, 
	color=color_clsb,
	alpha=0.5,
	label = '$p_{b+s}$'
)

# Set alpha area
x_fill = np.arange(-20, 4, 0.1)
y_fill = stats.norm.pdf(x_fill, mu, sigma)
plt.fill_between(
	x_fill,
	y_fill,
	0, 
	color=color_clb,
	alpha=0.5,
	hatch='///',
	label = '$p_b$', 
)



plt.legend(
	loc='upper left', 
	frameon=False,
	prop={'size': 13}

)

plt.show()

#plt.savefig("cls_parts.png")

#plt.close()
