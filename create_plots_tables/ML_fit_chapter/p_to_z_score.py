import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math

color_h0 = "purple"
color_p_value = "green"
color_norm_dist = "blue"
color_z_score = "black"
color_observed = "DarkOliveGreen"
mu = 0
variance = 1
sigma = math.sqrt(variance)
x = np.arange(-5, 5, 0.1)

fig, ax = plt.subplots()

#Plot normal distribution
plt.plot(
	x, stats.norm.pdf(x, mu, sigma), 
	color = color_norm_dist
)

# Set axis limits and properties
plt.axis(
	ymin= 0.0, 
	xmin = -3.5, 
	xmax = 3.5,
	ymax = 0.45
)

plt.tick_params(
    axis='both',         
    which='both', 
    direction="in",
    bottom=False, 
    top=False,
    #left=False,
    labelbottom=False, 
    labelleft=False
 )


ax.set_xlabel('$\mu$ parameter', loc='right', size = 12)
ax.set_ylabel('a.u.', loc='top', size = 12)

# Set line to indicate value under null hypothesis
plt.vlines(
	x=0.0, ymin = 0, ymax = stats.norm.pdf(0, mu, sigma), 
	ls='--', 
	color = color_h0
)
plt.text(
	0.0, 0.41, 
	'$\mu$ under $H_0$', 
	horizontalalignment='center',
	fontsize = 12, 
	color=color_h0
)

# Measured value
plt.vlines(
	x=1.5, ymin = 0, ymax = stats.norm.pdf(1.5, mu, sigma), 
	ls='--', 
	color = color_observed
)
plt.plot(
	1.5, 0, 
	marker="o", 
	markersize=10, 
	color=color_observed,
	clip_on=False,
	alpha = 0.9,
)
t='Observed\n value'
plt.text(
	-1, 0.05, 
	t, 
	horizontalalignment='center',
	fontsize = 12, 
	color=color_observed
)
plt.annotate(
	text="",	
	fontsize = 12, 
	xy=(1.46, 0.0018), 
	xytext=(-0.55, 0.055), 
	arrowprops=dict(arrowstyle='->', color = color_observed),
	color = color_observed
)

# Area with p-value
x_fill = np.arange(1.5, 3.5, 0.1)
y_fill = stats.norm.pdf(x_fill, mu, sigma)
plt.fill_between(
	x_fill,
	y_fill,
	0, 
	color=color_p_value,
	alpha=0.5
)
plt.annotate(
	text='p-value',	
	fontsize = 12, 
	xy=(2, stats.norm.pdf(2, mu, sigma)), 
	xytext=(2.3, 0.1), 
	arrowprops=dict(arrowstyle='->', color = color_p_value),
	color = color_p_value
)

# Z-score
plt.text(
	0.75, 0.09, 
	'Z score', 
	horizontalalignment='center',
	fontsize = 12, 
	color=color_z_score
)

plt.annotate(
	text='', 
	xy=(0,0.08), 
	xytext=(1.5,0.08), 
	arrowprops=dict(arrowstyle='<->', color=color_z_score)
)



#plt.show()
plt.savefig("z_score_figure.pdf")

plt.close()
