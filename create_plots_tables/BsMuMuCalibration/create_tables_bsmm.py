import json

decay = "BsMuMu"
control_channel = "JPsiPhi" if "Bs" in decay else "JPsiK"
f_text = open(f'table_bmm_calibration_{decay}.txt', 'w')
ibdt_vector = ["$[0, 0.25)$", "$[0.25, 0.4)$", "$[0.4, 0.5)$", 
				"$[0.5, 0.6)$", "$[0.6, 0.7)$", "$[0.7, 0.8)$", "$[0.8, 0.9)$", "$[0.9, 1]$"]
for i_year in [11, 16]:

	f_text.writelines(f'20{i_year} & 20{i_year+1} & 20{15 if i_year == 11 else 18}  \n')

	### YEAR I
	f_corr_yields_i = open(
		f"{decay}/{decay}_BDTFractions_MC{i_year}_8binsBDT_IPCHI2With{control_channel}AndnTracksCorrected.json"
	)
	dict_corr_rel_yields_i = json.load(f_corr_yields_i)
	corr_rel_yields_i = dict_corr_rel_yields_i[str(i_year)]

	f_corr_yields_1 = open(
		f"{decay}/{decay}_BDTFractions_MC{i_year+1}_8binsBDT_IPCHI2With{control_channel}AndnTracksCorrected.json"
	)
	dict_corr_rel_yields_1 = json.load(f_corr_yields_1)
	corr_rel_yields_1 = dict_corr_rel_yields_1[str(i_year+1)]

	f_corr_yields_2 = open(
		f"{decay}/{decay}_BDTFractions_MC{15 if i_year == 11 else 18}_8binsBDT_IPCHI2With{control_channel}AndnTracksCorrected.json"
	)
	dict_corr_rel_yields_2 = json.load(f_corr_yields_2)
	corr_rel_yields_2 = dict_corr_rel_yields_2[str(15 if i_year == 11 else 18)]

	for i_key in corr_rel_yields_2.keys():
		if int(i_key) == 0: 
			continue
		else:

			f_text.writelines(f'{ibdt_vector[int(i_key)-1]}')
			counter = 0 
			for i_dict in [corr_rel_yields_i, corr_rel_yields_1, corr_rel_yields_2]: #rel_yields, rel_trg, rel_pid, corr_rel_yields]:
				central_val = round(
					i_dict[i_key][0], 
					5 if counter == 2 else 3
				)
				stats_unc   = round(
					i_dict[i_key][1], 
					5 if counter == 2 else 3
				)
				syst_unc    = round(
					i_dict[i_key][2], 
					5 if counter == 2 else 3
				)
				f_text.writelines(f'& ${central_val} \pm {stats_unc} \pm {syst_unc}$ ')
				counter+=1
			f_text.writelines('\\ \n')
	f_text.writelines('\n \n')


