import json

decay = "BsKK"
f_text = open(f'table_bhh_calibration_{decay}.txt', 'w')
ibdt_vector = ["$[0, 0.25)$", "$[0.25, 0.4)$", "$[0.4, 0.5)$", 
				"$[0.5, 0.6)$", "$[0.6, 0.7)$", "$[0.7, 0.8)$", "$[0.8, 0.9)$", "$[0.9, 1]$"]

for i_year in [11, 12, 15, 16, 17, 18]:

	f_text.writelines(f'20{i_year} \n') #  & 20{i_year+1} \n')
	f_text.writelines(
		'BDT & $f_{\mathrm{BDT, data}}$ & $w_{\mathrm{BDT, Trg}}$ ' 
		'& $w_{\mathrm{BDT, PID}}$ & $f_{\mathrm{BDT}}$ \\ \n')
	#'& $f_{\mathrm{BDT, data}}$ & $w_{Trg}$  & $w_{PID}$ & $f_{\mathrm{BDT}}$') 

	### YEAR I
	f_pid_i = open(f"{decay}/relativePID/relativePIDEfficiencies_{decay}_{i_year}_8binsBDT.json")
	dict_rel_pid_i = json.load(f_pid_i)
	rel_pid_i = dict_rel_pid_i[str(i_year)]

	f_trg_i = open(f"{decay}/RelTrgEff/RelEff_{decay}MC{i_year}_8binsBDT.json")
	dict_rel_trg_i = json.load(f_trg_i)
	rel_trg_i = dict_rel_trg_i[str(i_year)]

	f_yields_i = open(f"{decay}/dataRelYields/RelFractions_{decay}_data{i_year}_8binsBDT.json")
	dict_rel_yields_i = json.load(f_yields_i)
	rel_yields_i = dict_rel_yields_i[str(i_year)]

	f_corr_yields_i = open(f"{decay}/BDTCorrectedFractions_{decay}{i_year}_FullCorrelationErrors_8binsBDT.json")
	dict_corr_rel_yields_i = json.load(f_corr_yields_i)
	corr_rel_yields_i = dict_corr_rel_yields_i[str(i_year)]


	# YEAR I+1
	"""
	f_pid = open(f"{decay}/relativePID/relativePIDEfficiencies_{decay}_{int(i_year+1)}_8binsBDT.json")
	dict_rel_pid = json.load(f_pid)
	rel_pid = dict_rel_pid[str(i_year+1)]

	f_trg = open(f"{decay}/RelTrgEff/RelEff_{decay}MC{i_year+1}_8binsBDT.json")
	dict_rel_trg = json.load(f_trg)
	rel_trg = dict_rel_trg[str(i_year+1)]

	f_yields = open(f"{decay}/dataRelYields/RelFractions_{decay}_data{int(i_year+1)}_8binsBDT.json")
	dict_rel_yields = json.load(f_yields)
	rel_yields = dict_rel_yields[str(i_year+1)]

	f_corr_yields = open(f"{decay}/BDTCorrectedFractions_{decay}{int(i_year+1)}_FullCorrelationErrors_8binsBDT.json")
	dict_corr_rel_yields = json.load(f_corr_yields)
	corr_rel_yields = dict_corr_rel_yields[str(i_year+1)]
	"""
	for i_key in rel_pid_i.keys():
		if int(i_key) == 0: 
			continue
		else:
			f_text.writelines(f'{ibdt_vector[int(i_key)-1]}')
			counter = 0 
			for i_dict in [rel_yields_i, rel_trg_i, rel_pid_i, corr_rel_yields_i,]: #rel_yields, rel_trg, rel_pid, corr_rel_yields]:
				central_val = round(
					i_dict[i_key][0], 
					5 if counter == 0 or counter == 2 else 3
				)
				stats_unc   = round(
					i_dict[i_key][1], 
					5 if counter == 0 or counter == 2 else 3
				)
				syst_unc    = round(
					i_dict[i_key][2], 
					5 if counter == 0 or counter == 2 else 3
				)
				f_text.writelines(f'& ${central_val} \pm {stats_unc} \pm {syst_unc}$ ')
				counter+=1
			f_text.writelines('\\ \n')
	f_text.writelines('\n \n')


