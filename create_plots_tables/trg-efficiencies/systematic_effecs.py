import json
import math

decay = "BuJpsiK"



eff_json = open(
	f"{decay}_TotalEfficiencies_TrgStrategy.json"
)
dict_eff = json.load(eff_json)
print(f"Opening file {decay}_TotalEfficiencies_TrgStrategy.json")

sys_effect = ["Signal shape", "Binning", "TISTOS", "Map", "JpsiK"]



for iyear in ["11", "12", "15", "16", "17", "18"]:
	print(f"YEAR {iyear}")
	eff_err_sys = dict_eff[iyear][0]
	eff_err_sys_effects = dict_eff[iyear][3]
	tot_sys = 0
	for i in range(0, len(eff_err_sys_effects)):
		tot_sys += (eff_err_sys_effects[i])**2
		print(f"Effect of {sys_effect[i]} in systematic total efficiency is {(eff_err_sys_effects[i]**2)*100/dict_eff[iyear][2]**2} %")
	print(f"systematic total computed = {math.sqrt(tot_sys)} and the one stored is = {dict_eff[iyear][2]}")
	print("\n\n")
