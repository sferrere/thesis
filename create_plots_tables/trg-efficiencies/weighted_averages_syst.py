
import numpy as np
import json


lumi_dict = {
	"11":1.11*7.,
	"12":2.08*8.,
	"15":0.33*13.,
	"16":1.67*13.,
	"17":1.71*13.,
	"18":2.19*13.
}
Run1_list = ["11","12"]
Run2_list = ["15","16","17","18"]



sys_effect = ["Signal shape", "Binning", "TISTOS", "Map", "JpsiK"]

lumi_run1 = lumi_dict["11"] + lumi_dict["12"]
lumi_run2 = lumi_dict["15"] + lumi_dict["16"] + lumi_dict["17"] + lumi_dict["18"] 

counter_run = 0
decay = "BdKPi"
for run in [Run1_list, Run2_list]:
  print(run)
  lumi_total = lumi_run1 if counter_run == 0 else lumi_run2
  print("lumi total = ", lumi_total)  

  #for decay in ["BsMuMu"]:#"BsMuMu", "BdMuMu", "BsMuMuGamma", "BuJPsiK", "BdKPi"]:
  eff_json = open(
    f"{decay}_TotalEfficiencies_TrgStrategy.json"
  )
  dict_eff = json.load(eff_json)
  print(f"Opening file {decay}_TotalEfficiencies_TrgStrategy.json")

  ## Absolute values
  eff_num, stat_num, sys_num = 0., 0., 0
  for year in run:
    eff_num += dict_eff[year][0]*lumi_dict[year]
    stat_num += (dict_eff[year][1]*lumi_dict[year])**2
    sys_num += (dict_eff[year][2]*lumi_dict[year])**2
  eff_run = eff_num/lumi_total
  eff_stat = np.sqrt(stat_num)/lumi_total
  eff_sys = np.sqrt(sys_num)/lumi_total     
  print(f"decay = {decay}")
  print(f"---> trg efficiency = {eff_run*100} +/- {eff_stat*100} +/- {eff_sys*100}")

  ## Now per systematic effect
  for i in range(0, len(dict_eff["11"][3])):
    #print("SYSTEMATIC ", sys_effect[i])
    sys_eff_num = 0.
    for year in run:
      #print(" YEAR ", year)
      sys_eff_num += (dict_eff[year][3][i]*lumi_dict[year])**2
      #print("  systematic in this year = ", dict_eff[year][3][i]*100)
    sys_eff = np.sqrt(sys_eff_num)/lumi_total
    print(f"Systematic effect: {sys_effect[i]} = {sys_eff*100}")


  counter_run += 1


