
import numpy as np

decay = "BsMuMu"

lumi_dict = {
	"11":1.11*7.,
	"12":2.08*8.,
	"15":0.33*13.,
	"16":1.67*13.,
	"17":1.71*13.,
	"18":2.19*13.
}
Run1_list = ["11","12"]
Run2_list = ["15","16","17","18"]

acceptance_eff = {
	"BsMuMu" : {
		"11" : [0.18377,  0.0003930],
		"12" : [0.186675, 0.0002298],
		"15" : [0.192955, 0.0003748],
		"16" : [0.1942,   0.000473814],
		"17" : [0.192725, 0.000428165],
		"18" : [0.1937, 0.0005],
    },
  	"BdMuMu" : {
        "11" : [0.1833,   0.000532],
        "12" : [0.186905, 0.000318198],
        "15" : [0.19282,  0.00041018 ],
        "16" : [0.193815, 0.000498523],
        "17" : [0.193635, 0.000491452],
        "18" : [0.1942,  0.0005],
    },
    "BdKPi" : {
        "11" : [0.17735, 0.00024],
        "12" : [0.18978, 0.00050956 ],
        "15" : [0.19589, 0.000371248],
        "16" : [0.1966,  0.000491452],
        "17" : [0.1968,  0.000459619],
        "18" : [0.1966,  0.0005],
    },
    "BuJPsiK" : {
        "11" : [0.1641,   0.00032783],
        "12" : [0.166605, 0.0004808 ],
        "15" : [0.17339,  0.00034],
        "16" : [0.17297,  0.00042],
        "17" : [0.17398,  0.00057],
        "18" : [0.17354,  0.00042],  
    },
    "BsMuMuGamma" : {
        "11" : [0.4738,  0.0013],
        "12" : [0.4738,  0.0013],
        "15" : [0.4741,  0.0013],
        "16" : [0.47325, 0.000919239],
        "17" : [0.4756,  0.000848528],
        "18" : [0.4720,  0.0009],
    }
 }


recsel_eff_dict = { 
    "BsMuMu" : {
    	"11" : [0.3378 , 0.0006 ],
       	"12" : [0.31487, 0.00032],
       	"15" : [0.3296 , 0.0005 ],
       	"16" : [0.33176, 0.00033],
       	"17" : [0.33198, 0.00033],
       	"18" : [0.3323 , 0.0005 ]
    },
    "BdMuMu" : {
       "11" : [0.3362 , 0.0007 ],
       "12" : [0.3095 , 0.0007 ],
       "15" : [0.3240 , 0.0004 ],
       "16" : [0.32616, 0.00033],
       "17" : [0.32600, 0.00033],
       "18" : [0.3340 , 0.0005 ],
    },
    "BsMuMuGamma" : {
       "11" : [0.1086 , 0.0008 ],
       "12" : [0.1055 , 0.0005 ],
       "15" : [0.1142 , 0.0006 ],
       "16" : [0.1155 , 0.0004 ],
       "17" : [0.1152 , 0.0004 ],
       "18" : [0.11630, 0.00029],
    },
    "BdKPi" : {
       "11" : [0.2593 , 0.0005 ],
       "12" : [0.24103, 0.00015],
       "15" : [0.25537, 0.00031],
       "16" : [0.25543, 0.00022],
       "17" : [0.25572, 0.00022],
       "18" : [0.25511, 0.00022],
    },
    "BuJPsiK" : {
       "11" : [0.1837 , 0.0004 ],
       "12" : [0.16648, 0.00017],
       "15" : [0.17370, 0.00026],
       "16" : [0.17757, 0.00019],
       "17" : [0.17759, 0.00012],
       "18" : [0.17745, 0.00012],
     }
 }
  

tracking_correction = { 
    "BsMuMu" : {
    	"11" : [1.0073, 0.0067],
       	"12" : [1.0063, 0.0061],
       	"15" : [0.9990, 0.0141],
       	"16" : [0.9911, 0.0116],
       	"17" : [0.9989, 0.0115],
       	"18" : [0.9984, 0.0116],
    },
    "BdMuMu" : {
       "11" : [1.0073, 0.0067],
       "12" : [1.0063, 0.0068],
       "15" : [0.9988, 0.0140],
       "16" : [0.9911, 0.0116],
       "17" : [0.9989, 0.0115],
       "18" : [0.9982, 0.0116],
    },
    # WARNING the following is copy paste from bsmumu 
    "BsMuMuGamma" : {
       "11" : [1.0073, 0.0067],
       "12" : [1.0063, 0.0061],
       "15" : [0.9990, 0.0141],
       "16" : [0.9911, 0.0116],
       "17" : [0.9989, 0.0115],
       "18" : [0.9984, 0.0116],
    },
    #"BsMuMuGamma" : {
    #   "11" : [1.0082, 0.0120],
    #   "12" : [1.0069, 0.0094],
    #   "15" : [0.9985, 0.0154],
    #   "16" : [0.9911, 0.0126],
    #   "17" : [0.9986, 0.0124],
    #   "18" : [0.9980, 0.0119],
    #},
    "BdKPi" : {
       "11" : [1.0069, 0.0191],
       "12" : [1.0062, 0.0189],
       "15" : [0.9987, 0.0227],
       "16" : [0.9911, 0.0211],
       "17" : [0.9988, 0.0211],
       "18" : [0.9982, 0.0211],
   	},
    "BuJPsiK" : {
       "11" : [1.0210, 0.0143],
       "12" : [1.0195, 0.0136],
       "15" : [0.9941, 0.0193],
       "16" : [0.9875, 0.0184],
       "17" : [0.9973, 0.0178],
       "18" : [0.9911, 0.0177],
  	}
}

print("GEOMETRICAL ACCEPTANCE")
for run in [Run1_list, Run2_list]:
	print(run)
	for decay in ["BsMuMu", "BdMuMu", "BsMuMuGamma", "BuJPsiK", "BdKPi"]:
		acc_eff_num, acc_stat_num, lumi_total = 0., 0., 0.
		recsel_eff_num, recsel_stat_num = 0., 0.
		for year in run:
			acc_eff_num += acceptance_eff[decay][year][0]*lumi_dict[year]
			acc_stat_num += (acceptance_eff[decay][year][1]*lumi_dict[year])**2
			lumi_total += lumi_dict[year]

			#recsel_eff_num += recsel_eff_dict[decay][year][0]*tracking_correction[decay][year][0]*lumi_dict[year]
			#recsel_stat_num += ((recsel_eff_dict[decay][year][1]*tracking_correction[decay][year][0]*lumi_dict[year])**2 +
			#			 		(recsel_eff_dict[decay][year][0]*tracking_correction[decay][year][1]*lumi_dict[year])**2)
		acc_eff = acc_eff_num/lumi_total
		acc_stat = np.sqrt(acc_stat_num)/lumi_total
		#recsel_eff = recsel_eff_num/lumi_total
		#recsel_stat = np.sqrt(recsel_stat_num)/lumi_total

		#tot_eff = acc_eff*recsel_eff
		#tot_stat = np.sqrt((acc_stat*recsel_eff)**2 + (acc_eff*recsel_stat)**2)
		print(f"decay = {decay}")
		print(f"---> acc efficiency = {acc_eff*100} +/- {acc_stat*100}")
		#print(f"---> recsel efficiency = {recsel_eff*100} +/- {recsel_stat*100}")
		#print(f"---> total efficiency = {tot_eff*100} +/- {tot_stat*100}")

print("RECSEL EFFICIENCIES")
for run in [Run1_list, Run2_list]:
	print(run)
	for decay in ["BsMuMu", "BdMuMu", "BsMuMuGamma", "BuJPsiK", "BdKPi"]:
		eff_num, stat_num, lumi_total = 0., 0., 0.
		eff_num_notrack, stat_num_notrack = 0., 0.
		for year in run:
			eff_num_notrack += recsel_eff_dict[decay][year][0]*lumi_dict[year]
			stat_num_notrack += (recsel_eff_dict[decay][year][1]*lumi_dict[year])**2

			eff_num += recsel_eff_dict[decay][year][0]*tracking_correction[decay][year][0]*lumi_dict[year]
			stat_num += ((recsel_eff_dict[decay][year][1]*tracking_correction[decay][year][0]*lumi_dict[year])**2 +
						 (recsel_eff_dict[decay][year][0]*tracking_correction[decay][year][1]*lumi_dict[year])**2)
			lumi_total += lumi_dict[year]
		eff = eff_num/lumi_total
		stat = np.sqrt(stat_num)/lumi_total

		print(f"With ntracks decay = {decay} ---> efficiency = {eff*100} +/- {stat*100}")

		eff_notrack = eff_num_notrack/lumi_total
		stat_notrack = np.sqrt(stat_num_notrack)/lumi_total

		print(f"No track decay = {decay} ---> efficiency = {eff_notrack*100} +/- {stat_notrack*100}")



print("RECSEL EFFICIENCIES")
for run in [Run1_list, Run2_list]:
	print(run)
	for decay in ["BsMuMu", "BdMuMu", "BsMuMuGamma", "BuJPsiK", "BdKPi"]:
		eff_num, stat_num, lumi_total = 0., 0., 0.
		for year in run:
			eff_num_notrack += recsel_eff_dict[decay][year][0]*lumi_dict[year]
			stat_num_notrack += (recsel_eff_dict[decay][year][1]*lumi_dict[year])**2

			eff_num += recsel_eff_dict[decay][year][0]*acceptance_eff[decay][year][0]*tracking_correction[decay][year][0]*lumi_dict[year]
			stat_num += ((recsel_eff_dict[decay][year][1]*acceptance_eff[decay][year][0]*tracking_correction[decay][year][0]*lumi_dict[year])**2 +
						 (recsel_eff_dict[decay][year][0]*acceptance_eff[decay][year][1]*tracking_correction[decay][year][0]*lumi_dict[year])**2 + 
						 (recsel_eff_dict[decay][year][0]*acceptance_eff[decay][year][0]*tracking_correction[decay][year][1]*lumi_dict[year])**2)
			lumi_total += lumi_dict[year]
		eff = eff_num/lumi_total
		stat = np.sqrt(stat_num)/lumi_total

		print(f"With ntracks decay = {decay} ---> efficiency = {eff*100} +/- {stat*100}")


