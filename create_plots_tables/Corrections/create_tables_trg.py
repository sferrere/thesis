import json

decay = "BdMuMu"
f_text = open(f'table_trigger_bdt_{decay}.txt', 'w')

for i_year in [11, 16]:

	f_text.writelines(f'20{i_year} & 20{i_year+1} & 20{15 if i_year == 11 else 18}  \n')

	### YEAR I
	f_corr_yields_i = open(
		f"Trigger_shape/{decay}MC{i_year}_Trigger_8binsBDTShape.json"
	)
	dict_corr_rel_yields_i = json.load(f_corr_yields_i)
	corr_rel_yields_i = dict_corr_rel_yields_i[str(i_year)]

	f_corr_yields_1 = open(
		f"Trigger_shape/{decay}MC{i_year+1}_Trigger_8binsBDTShape.json"
	)
	dict_corr_rel_yields_1 = json.load(f_corr_yields_1)
	corr_rel_yields_1 = dict_corr_rel_yields_1[str(i_year+1)]

	f_corr_yields_2 = open(
		f"Trigger_shape/{decay}MC{15 if i_year == 11 else 18}_Trigger_8binsBDTShape.json"
	)
	dict_corr_rel_yields_2 = json.load(f_corr_yields_2)
	corr_rel_yields_2 = dict_corr_rel_yields_2[str(15 if i_year == 11 else 18)]

	for i_key in corr_rel_yields_2.keys():
		if int(i_key) == 0: 
			continue
		else:
			f_text.writelines(f'{i_key} ')
			counter = 0 
			for i_dict in [corr_rel_yields_i, corr_rel_yields_1, corr_rel_yields_2]: #rel_yields, rel_trg, rel_pid, corr_rel_yields]:
				central_val = round(
					i_dict[i_key][0], 
					4 if counter == 2 else 3
				)
				stats_unc   = round(
					i_dict[i_key][1], 
					4 if counter == 2 else 3
				)
				f_text.writelines(f'& ${central_val} \pm {stats_unc}$ ')
				counter+=1
			f_text.writelines('\\ \n')
	f_text.writelines('\n \n')


